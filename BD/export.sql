-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Feb 11, 2021 at 04:43 PM
-- Server version: 5.7.26
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `pret`
--

-- --------------------------------------------------------

--
-- Table structure for table `acces_materiel`
--

CREATE TABLE `acces_materiel` (
  `id` int(11) NOT NULL,
  `nom_acces` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acces_materiel`
--

INSERT INTO `acces_materiel` (`id`, `nom_acces`) VALUES
(1, 'MIAGE'),
(2, 'TOUS'),
(3, 'ISRI');

-- --------------------------------------------------------

--
-- Table structure for table `dossier_pret`
--

CREATE TABLE `dossier_pret` (
  `id` int(11) NOT NULL,
  `id_etudiant` int(11) DEFAULT NULL,
  `id_equipement` int(11) DEFAULT NULL,
  `id_duree_pret` int(11) DEFAULT NULL,
  `id_statut` int(11) DEFAULT NULL,
  `id_famille` int(11) DEFAULT NULL,
  `attestation_assurance1` text,
  `attestation_assurance2` text,
  `attestation_assurance3` text,
  `attestation_assurance4` text,
  `carte_etudiant1` text,
  `carte_etudiant2` text,
  `carte_etudiant3` text,
  `carte_etudiant4` text,
  `date_demande` date DEFAULT NULL,
  `date_fin_souhaite` date DEFAULT NULL,
  `commentaire_etudiant` text,
  `commentaire_administration` text,
  `formation` varchar(32) DEFAULT NULL,
  `archive_equipement` tinytext,
  `groupe` tinyint(1) NOT NULL DEFAULT '0',
  `commentaire_informatique` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dossier_pret`
--

INSERT INTO `dossier_pret` (`id`, `id_etudiant`, `id_equipement`, `id_duree_pret`, `id_statut`, `id_famille`, `attestation_assurance1`, `attestation_assurance2`, `attestation_assurance3`, `attestation_assurance4`, `carte_etudiant1`, `carte_etudiant2`, `carte_etudiant3`, `carte_etudiant4`, `date_demande`, `date_fin_souhaite`, `commentaire_etudiant`, `commentaire_administration`, `formation`, `archive_equipement`, `groupe`, `commentaire_informatique`) VALUES
(1, 17, 34, 4, 7, 4, 'assurancerc-5f5bc0f15d78b.pdf', NULL, NULL, NULL, 'carteetud-5f5bc0f161b90.pdf', NULL, NULL, NULL, '2020-09-11', '2021-07-01', 'Je possède un PC fixe que je gardais l\'année dernière à Amiens et que j\'utilisais en semaine pour mes études. Or, maintenant, ayant trouvé mon alternance sur Rouen, je l\'ai ramené à Rouen car j\'en aurai besoin en semaine et aussi le week-end pour travailler et étudier. Je n\'en aurai donc plus à disposition dans mon logement amiénois, c\'est pourquoi je fais cette demande.', '', 'M1 MIAGE', NULL, 0, 'avec un sac à main'),
(2, 19, 2, 4, 7, 4, 'attestationassurance-5f5bf66abc530.pdf', NULL, NULL, NULL, 'universitedepicardiejulesverne-5f5bf66abf431.pdf', NULL, NULL, NULL, '2020-09-12', '2021-07-31', 'Bonjour, \r\nJ’ai des problèmes avec mon ordinateur actuel ( manque de performance et problème de batterie). Et aussi étant étudiant étranger et ne bénéficiant pas de la bourse française, les moyens dont je dispose ne me permettent pas d’acheter un ordinateur très très performant. Donc je viens solliciter un prêt histoire d’avoir un ordinateur performant qui me permettra de réaliser mes TD et TPS plus facilement et surtout pouvoir participer plus aisément aux cours à distance. \r\nCordialement,', '', 'M1 MIAGE', NULL, 0, 'Pas de sac'),
(3, 21, 31, 4, 7, 4, '119125191_326628285429798_5539141976597239612_n-5f5d3864a8ab7.pdf', NULL, NULL, NULL, 'new_document_1-5f5d3864ab1fe.pdf', NULL, NULL, NULL, '2020-09-12', '2022-06-24', 'Je demande un ordinateur portable car il m\'est impossible de travailler sur celui que j\'ai actuellement ( trop vieux, crash très souvent ) et il est surtout impossible à transporter pour les TD ( aucune batterie )', '', 'M1 MIAGE', NULL, 0, 'avec un sac au dos'),
(4, 24, 4, 4, 7, 4, 'doc_attdivers_20200914114817981_2261871m_1-5f5f4d6bbe656.pdf', NULL, NULL, NULL, 'w8idusax-5f5f4d6bbfef4.pdf', NULL, NULL, NULL, '2020-09-14', '2021-08-31', 'Ne possédant qu\'un ordinateur fixe j\'aimerais pouvoir suivre les cours sans être limité par mon matériel. Merci d\'avance', '', 'M1 MIAGE', NULL, 0, 'sans sac'),
(5, 25, 3, 1, 7, 4, 'mrh220091415521694-5f5f86134c883.pdf', NULL, NULL, NULL, 'generation-5f5f86134cbfe.pdf', NULL, NULL, NULL, '2020-09-14', '2021-08-01', 'ce prêt me permettra de bien suivre les travaux dirigés et les CM a distance car le mien est en mauvais etat.\r\nMerci', '', 'M1 MIAGE', NULL, 0, 'Avec un sac'),
(6, 25, NULL, 1, 4, 4, 'mrh220091415521694-5f5f861d873be.pdf', NULL, NULL, NULL, 'generation-5f5f861d8779c.pdf', NULL, NULL, NULL, '2020-09-14', '2021-08-01', 'ce prêt me permettra de bien suivre les travaux dirigés et les CM a distance car le mien est en mauvais etat.\r\nMerci', '', 'M1 MIAGE', NULL, 0, NULL),
(7, 25, NULL, 4, 4, 4, 'mrh220091415521694-5f5f8730f3f87.pdf', NULL, NULL, NULL, 'generation-5f5f8731000af.pdf', NULL, NULL, NULL, '2020-09-14', '2021-07-01', 'Ce prêt de matériel me permettra de bien suivre les cours de tds et de réaliser les travaux dirigés car le mien est en mauvais état.\r\nMerci', '', 'M1 MIAGE', NULL, 0, NULL),
(8, 29, 14, 1, 7, 6, 'generic_attestation_scolaire-5f60762853c75.pdf', NULL, NULL, NULL, 'carteetudiante-5f607628543ea.pdf', NULL, NULL, NULL, '2020-09-15', '2021-01-31', 'Je me permets de faire une demande de prêt pour me permettre d\'assurer au mieux le cours d\'administration au système d\'exploitation (ASE)', '', 'M1 MIAGE', NULL, 0, ''),
(9, 31, 129, 1, 6, 5, 'attestation_lieu_en_propriete-5f611c853403d.pdf', NULL, NULL, NULL, 'carte_etudiante_souhib_herizi-5f611c8536583.pdf', NULL, NULL, NULL, '2020-09-15', '2020-07-02', 'J\'ai choisi l\'option ASI mobile', '', 'M2 MIAGE - OSIE', NULL, 0, ''),
(11, 37, 13, 4, 7, 4, 'adheassurance-5f620efbc4cff.pdf', NULL, NULL, NULL, 'cartedetudiant-5f620efbc8007.pdf', NULL, NULL, NULL, '2020-09-16', '2021-07-31', 'je suis étudiante en master 1 , mon pc portable n\'est pas performant pour faire mes projet , et son chargeur ne marche plus', '', 'M1 MIAGE', NULL, 0, 'avec un sac a main usagé'),
(12, 36, 29, 4, 7, 4, 'attestationresponsabilitecivilepersonnelle-5f621e5e32580.pdf', NULL, NULL, NULL, 'camscanner09162020161408-5f621e5e36d1c.pdf', NULL, NULL, NULL, '2020-09-16', '2021-06-30', 'Pas d\'ordinateur sous windows.', '', 'M1 MIAGE', NULL, 0, 'sac à main'),
(13, 36, NULL, NULL, 5, 4, 'attestationresponsabilitecivilepersonnelle-5f621e669682b.pdf', NULL, NULL, NULL, 'camscanner09162020161408-5f621e6696b0d.pdf', NULL, NULL, NULL, '2020-09-16', '2021-06-30', 'Pas d\'ordinateur sous windows. ( Demande déjà fais, c\'est un bug en ré actualisant la page désolé)', '', 'M1 MIAGE', NULL, 0, NULL),
(14, 50, 124, 1, 6, 5, 'attestationresponsabilitecivilepersonnelle-5f69ec669c04c.pdf', NULL, NULL, NULL, 'carteetujosselinabel-5f69ec669c36d.pdf', NULL, NULL, NULL, '2020-09-22', '2021-02-28', 'ASI Mobile 2, mon macbook personnel ne fonctionne plus.', '', 'M2 MIAGE - SIO', NULL, 0, ''),
(15, 52, 130, 1, 6, 5, 'attestationrcscolaire1-5f6ba6831afc1.pdf', NULL, NULL, NULL, '120097963_4716805515011178_1648834623526476592_n-5f6ba6831f0fd.pdf', NULL, NULL, NULL, '2020-09-23', '2021-02-19', 'ASI Mobile 2', '', 'M2 MIAGE - SIO', NULL, 0, ''),
(16, 48, 140, 1, 6, 5, 'scolaire-5f6ce5a2d6ee3.pdf', NULL, NULL, NULL, 'carteetudiante-5f6ce5a2dc5be.pdf', NULL, NULL, NULL, '2020-09-24', '2021-08-31', 'Prêt pour le cours de SI Mobile 2', '', 'M2 MIAGE - OSIE', NULL, 0, ''),
(17, 51, 128, 1, 6, 5, 'attestation_responsabilite_civile-5f6dc4cf2d3eb.pdf', NULL, NULL, NULL, 'certificat_scolarite-5f6dc4cf2faca.pdf', NULL, NULL, NULL, '2020-09-25', '2021-08-30', 'Besoin d\'un ordinateur apple dans une matière car le développement se fait exclusivement dessus', '', 'M2 MIAGE - OSIE', NULL, 0, ''),
(18, 65, 131, 1, 6, 5, 'attestationdassurancescolaire-5f6dc8247806b.pdf', NULL, NULL, NULL, 'carteetudiant-5f6dc824910a4.pdf', NULL, NULL, NULL, '2020-09-25', '2020-08-31', 'Cours ASI MOBILE iOS', '', 'M2 MIAGE - SIO', NULL, 0, ''),
(19, 66, 126, 1, 6, 5, 'macif_null-5f6dc99b801a9.pdf', NULL, NULL, NULL, 'img_20191218_074609converted-5f6dc99b80a1a.pdf', NULL, NULL, NULL, '2020-09-25', '2021-08-31', 'Ordinateur portable MAC nécéssaire pour le cours d\'ASI Mobile 2', '', 'M2 MIAGE - SIO', NULL, 0, ''),
(20, 47, NULL, 1, 4, 4, 'attestationjeune_2-5f6f07eb925d0.pdf', NULL, NULL, NULL, 'carte_etudiant_upjv-5f6f07eb929e7.pdf', NULL, NULL, NULL, '2020-09-26', '2021-07-01', 'J\'ai besoin de ce kit ASE pour l\'UE \"ADMINISTRATION DES SE\" auquel je participe.', '', 'M1 MIAGE', NULL, 0, NULL),
(21, 38, 16, 1, 7, 6, 'attestations_noiret-5f706f816d603.pdf', NULL, NULL, NULL, 'carteetudiantnoiret-5f706f816ef03.pdf', NULL, NULL, NULL, '2020-09-27', '2021-08-01', 'Cours ASE (je ne sais pas quelle date de fin mettre)', '', 'M1 MIAGE', NULL, 0, ''),
(22, 33, 15, 1, 7, 6, 'assurantetudiant-5f7104ee53a66.pdf', NULL, NULL, NULL, 'carteetud-5f7104ee562e0.pdf', NULL, NULL, NULL, '2020-09-27', '2021-02-06', 'Kit nécessaire pour le cour ASE', '', 'M1 MIAGE', NULL, 0, ''),
(23, 23, NULL, 1, 4, 4, 'responsabilitecivil-5f72068f9e7dd.pdf', NULL, NULL, NULL, 'certifiicatsscolaire-5f72068fa41ab.pdf', NULL, NULL, NULL, '2020-09-28', '2021-09-01', 'Dans le cadre de mon choix d\'options administration des SE.\r\nJ\'aurais besoin du matériel correspondant.\r\n\r\nMerci de votre compréhension,\r\nCordialement\r\n\r\nMevlüt KEKLIK', '', 'M1 MIAGE', NULL, 0, NULL),
(24, 67, NULL, 1, 4, 5, 'attestation-5f723e4eef6b6.pdf', NULL, NULL, NULL, 'carteetudiant-5f723e4ef01c4.pdf', NULL, NULL, NULL, '2020-09-28', '2021-08-31', 'Option Info_19 ASI Mobile 2', '', 'M2 MIAGE - OSIE', NULL, 0, NULL),
(25, 34, 40, 4, 7, 4, 'attestationassurancealexisdefontaine-5f72d2aa72bba.pdf', NULL, NULL, NULL, 'carteetudiantalexisdefontaine-5f72d2aa7347b.pdf', NULL, NULL, NULL, '2020-09-29', '2021-06-01', 'Ne possède pas le matériel suffisant pour suivre les cours et travailler dans des conditions normales, mais compte en acheter si trouve une alternance durant l\'année', '', 'M1 MIAGE', NULL, 0, 'avec un sac a dos bon etat'),
(26, 70, 101, 1, 6, 6, 'attestationlocative-5f7340b6f3833.pdf', NULL, NULL, NULL, 'carte_etudiant-5f7340b6f417c.pdf', NULL, NULL, NULL, '2020-09-29', '2021-08-31', 'Besoin pour faire le cours d\'ASE', '', 'M1 MIAGE', NULL, 0, ''),
(27, 22, 30, 4, 7, 4, 'attestationdroits-5f744e4d6aa2d.pdf', NULL, NULL, NULL, '20200930_110847-5f744e4d6e0ac.pdf', NULL, NULL, NULL, '2020-09-30', '2022-06-30', 'Je n\'ai pas de pc portable, ainsi pour une raison pratique qui me permettra de regrouper tout mon travail sur un seul pc, je souhaite faire un prêt pour un pc portable.', '', 'M1 MIAGE', NULL, 0, 'avec un sac à main\r\n'),
(28, 54, 134, 1, 6, 5, 'attestation_maziere-5f760aa6c94c0.pdf', NULL, NULL, NULL, 'carte_etudiant_maziere-5f760aa6cde36.pdf', NULL, NULL, NULL, '2020-10-01', '2021-06-01', 'J\'ai besoin d\'un MacBook pour suivre l\'UE ASI Mobile 2.\r\nMerci', '', 'M2 MIAGE - SIO', NULL, 0, ''),
(29, 49, 28, 4, 7, 4, 'ca_attestation_rc_vie_privee_habitation-5f7896bc04f8f.pdf', NULL, NULL, NULL, 'carteetudiante-5f7896bc0bda6.pdf', NULL, NULL, NULL, '2020-10-03', '2021-07-02', 'Mon ordinateur portable est beaucoup trop lent (genre vraiment, il bug même pour ouvrir l\'explorateur).\r\nL\'attestation d\'assurance est celle de mon père qui me couvre également étant donné que j\'habite chez lui.', '', 'M2 MIAGE - OSIE', NULL, 0, 'avec sac à main bon état'),
(30, 68, 135, 1, 6, 5, 'attestation26-5f78ab854580a.pdf', NULL, NULL, NULL, 'cartee_recto_donatien_segard-5f78ab8545d23.pdf', NULL, NULL, NULL, '2020-10-03', '2021-02-28', 'Besoin d\'un ordinateur portable Apple pour suivre l\'option ASI mobile 2 de la seconde année de master MIAGE', '', 'M1 MIAGE', NULL, 0, ''),
(31, 73, 45, 4, 7, 4, 'attestationdassurancehabitati-5f798ea03c6b1.pdf', NULL, NULL, NULL, 'img_20201004_1055391-5f798ea03eef8.pdf', NULL, NULL, NULL, '2020-10-04', '2021-09-10', 'Bonjour,\r\nDans le cadre de ma formation (Master 2 MIAGE) je souhaiterais acquérir un ordinateur portable en prêt.\r\nEn effet celui-ci me permettrait d\'effectuer les travaux demandés en présentiel ainsi que les projets.\r\nDans l\'attente d\'une réponse de votre part.\r\nEn vous remerciant.\r\nCordialement,', '', 'M2 MIAGE - OSIE', NULL, 0, '1 sac à main'),
(32, 71, 125, 1, 6, 5, 'assurance-5f7c6462a401f.pdf', NULL, NULL, NULL, 'carteetudiant-5f7c6462ad028.pdf', NULL, NULL, NULL, '2020-10-06', '2021-09-01', 'Pour le cours de Mr Guerin : ASI MOBILE 2', '', 'M2 MIAGE - SIO', NULL, 0, ''),
(33, 76, 53, 4, 7, 4, 'assurance-5f7c9ddae9971.pdf', NULL, NULL, NULL, 'carteetudiante-5f7c9ddae9dbc.pdf', NULL, NULL, NULL, '2020-10-06', '2021-07-04', 'Bonjour, \r\n\r\nMon ordinateur portable actuel n\'est plus capable de tenir la charge (malgré un changement de batterie) et est de plus en plus lent dans l\'exécution des programmes ou applications utilisées au sein de mon cursus.\r\n\r\nN\'ayant, pour le moment, pas les ressources financières nécessaires pour m\'en acheter un autre, je souhaiterais faire une demande de prêt pour un ordinateur portable Windows. Je devrais alors y installer Photoshop, un serveur web, et tous les autres logiciels nécessaires à la poursuite de ce Master 2. De plus, je serais alors capable d\'utiliser cet ordinateur dans les salles où il n\'y a pas ou peu de prise, puisqu\'il devrait enfin tenir la charge.\r\n\r\nJ\'ai fixé la date de fin du prêt au 4 juillet 2021, date à laquelle les cours à l\'université sont censés se terminer, mais j\'imagine que je pourrai rendre l\'ordinateur dès que je pourrai en acheter un neuf.\r\n\r\nMerci d\'avance !', '', 'M2 MIAGE - SIO', NULL, 0, ''),
(39, 82, 123, 1, 7, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-10-08', NULL, NULL, 'Demande papier', 'M1 ISRI', NULL, 0, ''),
(40, 83, 89, 1, 7, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-10-08', NULL, NULL, 'Demande papier', 'M1 ISRI', NULL, 0, ''),
(41, 74, 139, 1, 6, 5, 'attestationjeune_2-5f7f39d041d4b.pdf', NULL, NULL, NULL, 'carteetudiant-5f7f39d044611.pdf', NULL, NULL, NULL, '2020-10-08', '2021-07-02', 'J\'ai besoin d\'un Mac pour l\'UE INFO_19 Architecture des Système d\'Information Mobile.', '', 'M2 MIAGE - SIO', NULL, 0, ''),
(42, 28, 133, 1, 6, 5, 'assurance_habitation-5f7f83788a4a0.pdf', NULL, NULL, NULL, 'certiscolarite-5f7f837891efc.pdf', NULL, NULL, NULL, '2020-10-08', '2021-08-30', 'j\'ai besoin d\'un ordinateur Apple indispensable dans le cadre de l\'UE ASI Mobile 2\r\nMerci', '', 'M2 MIAGE - SIO', NULL, 0, ''),
(43, 84, 43, 4, 7, 4, 'attestation-5f80329817706.pdf', NULL, NULL, NULL, 'cartedetudiant2-5f80329818233.pdf', NULL, NULL, NULL, '2020-10-09', '2020-10-12', 'Mon ordinateur est très lent lors de lancements de déférents programmes (Discord, Éclipse, navigateur ...)', '', 'M1 MIAGE', NULL, 0, 'pas de sac'),
(44, 23, 17, 1, 6, 6, 'responsabilitecivil-5f857df8dd31f.pdf', NULL, NULL, NULL, 'certifiicatsscolaire-5f857df8e811e.pdf', NULL, NULL, NULL, '2020-10-13', '2020-10-13', 'merci', '', 'M1 MIAGE', NULL, 0, ''),
(45, 31, NULL, 4, 4, 4, 'attestation_lieu_en_propriete5f611c853403d-5f8ea7da98620.pdf', NULL, NULL, NULL, 'carte_etudiante_souhib_herizi5f611c8536583-5f8ea7da98a49.pdf', NULL, NULL, NULL, '2020-10-20', '2021-07-03', 'J\'ai un pc windows qui a un problème au niveau du disque dur. De ce fait, il est très lent et m\'empêche de travailler convenablement.', '', 'M2 MIAGE - OSIE', NULL, 0, NULL),
(46, 49, NULL, 1, 4, 4, 'ca_attestation_rc_vie_privee_habitation5f7896bc04f8f-5f8eda530686c.pdf', NULL, NULL, NULL, 'carteetudiante5f7896bc0bda6-5f8eda5306b9a.pdf', NULL, NULL, NULL, '2020-10-20', '2021-07-02', 'Demande de prêt recréée à la demande de M. Vaniet car j\'ai dû changer de machine 2 fois (elles ne fonctionnaient pas) et il lui est impossible de changer le matériel assigné sur la demande initiale. Le numéro de la machine est : 46613', '', 'M2 MIAGE - OSIE', NULL, 0, NULL),
(47, 75, NULL, 1, 4, 4, 'assurancehabitation20202021converti-5f8eee2b13ae6.pdf', NULL, NULL, NULL, 'image21converti-5f8eee2b148f9.pdf', NULL, NULL, NULL, '2020-10-20', '2021-06-30', 'pour suivre le cours d\'ASI mobile 2 (besoin d\'un ordinateur apple)', '', 'M2 MIAGE - SIO', NULL, 0, NULL),
(48, 75, NULL, 1, 4, 5, 'assurancehabitation20202021converti-5f91781b58028.pdf', NULL, NULL, NULL, 'image21converti-5f91781b58f72.pdf', NULL, NULL, NULL, '2020-10-22', '2021-06-30', 'ASI mobile 2', '', 'M2 MIAGE - SIO', NULL, 0, NULL),
(49, 86, NULL, NULL, 2, 4, 'attestationattmrhrcscoextsco0128102020-5f9a905053803.pdf', NULL, NULL, NULL, '6891091zqelpme-5f9a90506a60b.pdf', NULL, NULL, NULL, '2020-10-29', '2021-06-01', 'Besoin pour l\'UE ASI 2 car problème avec mon pc perso', NULL, 'M2 MIAGE - OSIE', NULL, 0, NULL),
(50, 22, NULL, NULL, 1, 8, 'attestationdroits5f744e4d6aa2d-5fa14dfaa4857.pdf', NULL, NULL, NULL, '20200930_1108475f744e4d6e0ac-5fa14dfaa4f2c.pdf', NULL, NULL, NULL, '2020-11-03', '2020-11-05', 'Besoin pour cours, \"Découverte de la recherche\"', NULL, 'M1 MIAGE', NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `duree_pret`
--

CREATE TABLE `duree_pret` (
  `id` int(11) NOT NULL,
  `nom` varchar(32) NOT NULL,
  `date_fin` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `duree_pret`
--

INSERT INTO `duree_pret` (`id`, `nom`, `date_fin`) VALUES
(1, 'Semestre 1', '2020-03-01'),
(2, 'Semestre 2', '2021-09-01'),
(4, 'Annuel 2020-21', '2021-09-10');

-- --------------------------------------------------------

--
-- Table structure for table `equipement`
--

CREATE TABLE `equipement` (
  `id` int(11) NOT NULL,
  `id_acces_materiel` int(11) NOT NULL,
  `id_type_materiel` int(11) NOT NULL,
  `id_statut_materiel` int(11) NOT NULL,
  `etat` varchar(255) NOT NULL,
  `code_barre` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `equipement`
--

INSERT INTO `equipement` (`id`, `id_acces_materiel`, `id_type_materiel`, `id_statut_materiel`, `etat`, `code_barre`) VALUES
(2, 2, 2, 1, 'Dell Precision Bon état', '43309'),
(3, 2, 1, 1, 'latitude 5280', '43286'),
(4, 2, 2, 1, 'dell precision', '43308'),
(5, 2, 8, 2, 'core i5 13\" NS SC02VJ0CUHV2J', '43312'),
(6, 2, 8, 2, 'core i5 13\" NS SC02VJ0CTHV2J', '43313'),
(7, 2, 8, 2, 'core i5 13\" NS SC02VJ015HV2J', '43314'),
(8, 2, 8, 2, 'core i5 13\" NS SC02VJ06NHV2J', '43315'),
(9, 2, 8, 2, 'Aire core i5 13\" NS C1MRW5F4H3QF', '40689'),
(10, 2, 8, 2, 'Retina core NS sc02rc2t9fvh6', '36635'),
(11, 2, 8, 2, 'Retina core NS sc02qfrkafvh', '37020'),
(12, 2, 8, 2, 'Bon état', '31031'),
(13, 2, 1, 1, 'I7 7gen vpro', '43307'),
(14, 2, 9, 1, 'complet', 'k33'),
(15, 2, 9, 1, 'complet', 'k34'),
(16, 2, 9, 1, 'complet', 'k39'),
(17, 2, 9, 1, 'complet', 'k21'),
(18, 2, 9, 2, 'complet', 'k23'),
(19, 2, 9, 2, 'complet', 'k24'),
(20, 2, 9, 2, 'complet', 'k25'),
(21, 2, 7, 2, 'ok', '40494'),
(22, 2, 7, 2, 'ok', '40696'),
(23, 2, 7, 2, 'ok', '40495'),
(24, 2, 7, 2, 'ok', '37151'),
(25, 2, 1, 2, 'Dell lattitude 5280 année 2018', '46665'),
(26, 2, 1, 2, 'Dell lattitude 5280 année 2018', '46624'),
(27, 2, 1, 2, 'Dell lattitude 5280 année 2018', '46624'),
(28, 2, 1, 1, 'Dell lattitude 5280 année 2018', '46625'),
(29, 2, 1, 1, 'Dell lattitude 5280 année 2018', '43288'),
(30, 2, 1, 1, 'Dell lattitude 5280 année 2018', '43281'),
(31, 2, 1, 1, 'Dell lattitude 5280 année 2018', '43282'),
(32, 2, 1, 2, 'Dell lattitude 5280 année 2018', '43283'),
(33, 2, 1, 2, 'Dell lattitude 5280 année 2018', '43284'),
(34, 2, 1, 1, 'Dell lattitude 5280 année 2018', '43285'),
(35, 2, 1, 2, 'Dell lattitude 5280 année 2018', '43286'),
(36, 2, 1, 2, 'Dell lattitude 5280 année 2018', '43287'),
(37, 2, 1, 2, 'Dell lattitude 5280 année 2018', '43422'),
(38, 2, 1, 2, 'Dell lattitude 5290 année 2019', '46609'),
(39, 2, 1, 2, 'Dell lattitude 5290 année 2019', '46610'),
(40, 2, 1, 1, 'Dell lattitude 5290 année 2019', '46611'),
(41, 2, 1, 2, 'Dell lattitude 5290 année 2019', '46612'),
(42, 2, 1, 2, 'Dell lattitude 5290 année 2019', '46613'),
(43, 2, 1, 1, 'Dell lattitude 5290 année 2019', '46614'),
(44, 2, 1, 2, 'Dell lattitude 5290 année 2019', '46615'),
(45, 2, 1, 1, 'Dell lattitude 5290 année 2019', '46616'),
(46, 2, 7, 2, 'Zbook 15', '37030'),
(47, 2, 2, 2, 'dell precision 3520', '43307'),
(48, 2, 2, 2, 'dell precision 3520', '43308'),
(49, 2, 2, 2, 'dell precision 3520', '43309'),
(50, 2, 2, 2, 'dell precision 3520', '43310'),
(51, 2, 2, 2, 'dell precision 3520', '43311'),
(52, 2, 7, 2, 'Zbook 15', '40496'),
(53, 2, 7, 1, 'Zbook 15', '40695'),
(54, 2, 7, 2, 'Zbook 15', '36636'),
(55, 2, 7, 2, 'Zbook 15', '36616'),
(56, 2, 7, 2, 'Zbook 15', '43933'),
(57, 2, 7, 4, 'Zbook 15', '43932'),
(58, 2, 7, 2, 'Zbook 15', '43930'),
(59, 2, 7, 2, 'Zbook 15', '43931'),
(60, 2, 7, 2, 'Zbook 15', '43929'),
(61, 2, 7, 2, 'Zbook 15', '43926'),
(62, 2, 7, 2, 'Zbook 15', '43925'),
(63, 2, 7, 2, 'Zbook 15', '43928'),
(64, 2, 7, 2, 'Zbook 15', '43924'),
(65, 2, 7, 2, 'Zbook 15 HS', '43927'),
(66, 2, 8, 2, 'ok', '40505'),
(67, 2, 8, 2, 'ok', '40506'),
(68, 2, 8, 2, 'ok', '46556'),
(69, 2, 7, 2, 'Zbook 15', '36611'),
(70, 2, 7, 2, 'Zbook 15', '36612'),
(71, 2, 7, 2, 'Zbook 15', '36614'),
(72, 2, 7, 2, 'Zbook 15', '36614'),
(73, 2, 7, 2, 'Zbook 15', '36613'),
(74, 2, 7, 2, 'Zbook 15', '36613'),
(75, 2, 7, 2, 'Zbook 15', '36615'),
(76, 2, 7, 2, 'Zbook 15', '36616'),
(77, 2, 7, 2, 'Zbook 15', '37012'),
(78, 2, 8, 2, 'Aire core', '43872'),
(79, 2, 8, 2, 'core i5 13', '46558'),
(80, 2, 9, 2, 'complet', 'k01'),
(81, 2, 9, 2, 'complet', 'k02'),
(82, 2, 9, 2, 'complet', 'k03'),
(83, 2, 9, 2, 'complet', 'k04'),
(84, 2, 9, 2, 'complet', 'k05'),
(85, 2, 9, 2, 'complet', 'k06'),
(86, 2, 9, 2, 'complet', 'k07'),
(87, 2, 9, 2, 'complet', 'k08'),
(88, 2, 9, 2, 'complet', 'k09'),
(89, 2, 9, 1, 'complet', 'k10'),
(90, 2, 9, 2, 'complet', 'k11'),
(91, 2, 9, 2, 'complet', 'k12'),
(92, 2, 9, 2, 'complet', 'k13'),
(93, 2, 9, 2, 'complet', 'k14'),
(94, 2, 9, 2, 'complet', 'k15'),
(95, 2, 9, 2, 'complet', 'k16'),
(96, 2, 9, 2, 'complet', 'k17'),
(97, 2, 9, 2, 'complet', 'k18'),
(98, 2, 9, 2, 'complet', 'k19'),
(99, 2, 9, 2, 'complet', 'k20'),
(100, 2, 9, 2, 'complet', 'k22'),
(101, 2, 9, 1, 'complet', 'k26'),
(102, 2, 9, 2, 'complet', 'k27'),
(103, 2, 8, 2, 'complet', 'k28'),
(104, 2, 9, 2, 'complet', 'k29'),
(105, 2, 9, 2, 'complet', 'k30'),
(106, 2, 9, 2, 'complet', 'k31'),
(107, 2, 9, 2, 'complet', 'k32'),
(108, 2, 9, 2, 'complet', 'k35'),
(109, 2, 9, 2, 'complet', 'k36'),
(110, 2, 9, 2, 'complet', 'k37'),
(111, 2, 9, 2, 'complet', 'k38'),
(112, 2, 9, 2, 'complet', 'k40'),
(113, 2, 9, 2, 'complet', 'k41'),
(114, 2, 9, 2, 'complet', 'k42'),
(115, 2, 9, 2, 'complet', 'k43'),
(116, 2, 9, 2, 'complet', 'k44'),
(117, 2, 9, 2, 'complet', 'k45'),
(118, 2, 9, 2, 'complet', 'k46'),
(119, 2, 9, 2, 'complet', 'k47'),
(120, 2, 9, 2, 'complet', 'k48'),
(121, 2, 9, 2, 'complet', 'k49'),
(122, 1, 9, 2, 'complet', 'k50'),
(123, 2, 9, 1, 'complet', 'k00'),
(124, 1, 8, 1, 'avec boite', '46622'),
(125, 1, 8, 1, 'avec boite', '35520'),
(126, 1, 8, 1, 'avec boite', '35511'),
(127, 1, 8, 2, 'avec boite', '35511'),
(128, 1, 8, 1, 'avec boite', '35513'),
(129, 1, 8, 1, 'avec boite', '35514'),
(130, 1, 8, 1, 'avec boite', '35517'),
(131, 1, 8, 1, 'avec boite', '35519'),
(132, 1, 8, 2, 'avec boite', '35519'),
(133, 1, 8, 1, 'avec boite', '35518'),
(134, 1, 8, 1, 'avec boite', '35515'),
(135, 1, 8, 1, 'avec boite', '35510'),
(136, 1, 8, 2, 'avec boite', '35512'),
(137, 1, 8, 2, 'avec boite', '35521'),
(138, 1, 8, 2, 'avec boite', '47259'),
(139, 1, 8, 1, 'avec boite', '47621'),
(140, 1, 8, 1, 'avec boite', '46623');

-- --------------------------------------------------------

--
-- Table structure for table `etudiant`
--

CREATE TABLE `etudiant` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `numero` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `formation` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `activate` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `etudiant`
--

INSERT INTO `etudiant` (`id`, `nom`, `prenom`, `numero`, `formation`, `email`, `password`, `roles`, `activate`) VALUES
(17, 'BAUDU', 'Vincent', '21901837', 'M1 MIAGE', 'vincent.baudu@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$vvwGaawsjbt3uVSpRwUIDQ$AQSmyMGXH5SV/w3y0xydzi4dReXKks0dYnxmPPsBKbU', '[\"ROLE_ETUDIANT\"]', 1),
(19, 'Barry', 'Mamadou Binnet', '21707394', 'M1 MIAGE', 'mamadou.binnet.barry@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$EuMgGJbDIcRHX2XZ8ErcFA$TjqqQbD41Cpl9J8iXezhOwM/8ZR71CaxmzzTcG11w9Y', '[\"ROLE_ETUDIANT\"]', 1),
(21, 'Docquois', 'Morgane', '21508613', 'M1 MIAGE', 'morgane.docquois@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$9FsU3fV23jvU22qi2+sR3w$U2XFcUnFY4FzVd0EAKQEscRqDntDKBsQL4G7NmuipWY', '[\"ROLE_ETUDIANT\"]', 1),
(22, 'Dupont', 'Nathan', '21705439', 'M1 MIAGE', 'nathan60510@hotmail.fr', '$argon2id$v=19$m=65536,t=4,p=1$DygEU/MvmhEsQzqdPfEqMg$qTymPe3y6SF0IM6krRl9T7RePgZois5foZFXuAZTCd0', '[\"ROLE_ETUDIANT\"]', 1),
(23, 'keklik', 'mevlut', '21704467', 'M1 MIAGE', 'mevlut.keklik@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$Gy+r5QcCQx3jhUgea9Bu0Q$gu/hqchPuhYNKd0+6TXDdTmsUXBMibO5hUC8V77L4VI', '[\"ROLE_ETUDIANT\"]', 1),
(24, 'Cagnache', 'Hippolyte', '21701868', 'M1 MIAGE', 'hippolyte.cagnache@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$0BEPNAlasRXgi5Vxgo+JEg$glMLk985AQee66cPiEKEjW/RJRwaBzHjzikzENspCJI', '[\"ROLE_ETUDIANT\"]', 1),
(25, 'Traore', 'abdoul aziz', '21707033', 'M1 MIAGE', 'abdoul.aziz.traore@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$R1aArCqA4BTUdS8KzT6FDg$OpH/mml0a8hscRl6suYZDBqjZmK7s6IOsr0srn36Ba4', '[\"ROLE_ETUDIANT\"]', 1),
(26, 'CHEVRIAUX', 'Quentin', '21100151', 'M1 MIAGE', 'quentin.chevriaux@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$MiCMfevzY4J/HN/jYJ8fwA$pla6xlahkAxwJZP/i5E1RCjDreT94Yv3hxRWiEXOHoM', '[\"ROLE_ETUDIANT\"]', 1),
(27, 'Meteyer', 'Théo', '21919957', 'M1 MIAGE', 'theo.meteyer@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$1Aku7Wr0kCPn/9fM0fBUww$SYoe+IukmDRZkzPeJqAFs7fg6zmAOXaFlokccmM+ysU', '[\"ROLE_ETUDIANT\"]', 1),
(28, 'Souleymane Yacouba', 'Maman Nassirou', '21808246', 'M2 MIAGE - SIO', 'maman.nassirou.souleymane.yacouba@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$6cG76OHR9l6Qbo36dRyBEw$Dd7Ww0tCmj/2JvylmwARYeZlyHWup+0rgz3TyiLH9Ts', '[\"ROLE_ETUDIANT\"]', 1),
(29, 'Vautherot', 'Corentin', '21703453', 'M1 MIAGE', 'corentin.vautherot@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$+7mBINXSq67CRA5ZG3kWxg$aOAIiJZ6/o00ogS5p0cEePnNli0W8rYcTCYH29jydM8', '[\"ROLE_ETUDIANT\"]', 1),
(30, 'benaouf', 'cherif', '21708581', 'M1 MIAGE', 'cherif.benaouf@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$UvV+0SlxPtyQqpUfJAA1YQ$JB2QrgBJspy93Rh6zb6749mGgMfu8en4dUtygQKp0N0', '[\"ROLE_ETUDIANT\"]', 1),
(31, 'Herizi', 'Souhib', '21400196', 'M2 MIAGE - OSIE', 'souhib.herizi@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$tEeZV7Ry07H7er5AdLa+Zw$7Dm6yz+3XyaAfr1Vb3gC5rht+H4781BDYdI4qnzoOYE', '[\"ROLE_ETUDIANT\"]', 1),
(33, 'Relmy', 'Lucas', '21701934', 'M1 MIAGE', 'lucas.relmy@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$uAQ2wvv/nq7lspB7qjr+QQ$rLQSOnNAN8QM9AkFjwJ4XHksaBr7Y3jbIBRpQiYBzGU', '[\"ROLE_ETUDIANT\"]', 1),
(34, 'Défontaine', 'Alexis', '21602204', 'M1 MIAGE', 'alexis.defontaine@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$kooo/zCuDU0/1xNMNHRgkg$c3ueQ8jTrrk7DmbtL7rw50NL53g7lI4Um3ypz5c8qQw', '[\"ROLE_ETUDIANT\"]', 1),
(35, 'MOKHTARI', 'Sanae', '21808043', 'M2 MIAGE - SIO', 'sanae.mokhtari@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$rRt7BgS/l67ku76EAUZA1Q$EdfHFEfYQjDO6gF+pBLP1OrpDx33xMYMvvLLxijrLlw', '[\"ROLE_ETUDIANT\"]', 1),
(36, 'DE JESUs', 'Pauline', '21400252', 'M1 MIAGE', 'pauline.dejesus@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$Gh0jp29d0GV8g2/JDLS4kQ$W2u/Vd6Q+c1o56L1H7XGUWxKlC+RowkOH+pnNrsq7IU', '[\"ROLE_ETUDIANT\"]', 1),
(37, 'ABOURRI', 'Chaymae', '21807096', 'M1 MIAGE', 'chaymae.abourri@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$IqyYEPx/MgqwFWgi4xroWQ$RBYuV9ocxsrvBWm3uvVQY3gqXYXYf713hV/daUaZuzM', '[\"ROLE_ETUDIANT\"]', 1),
(38, 'Noiret', 'Léo', '21310180', 'M1 MIAGE', 'leo.noiret@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$2krT0xTdMmmOQyH36Un4BA$F4uxg5Av9SZT3s1mHpOu3Mkwqil7nO/8vRD6uyD1eho', '[\"ROLE_ETUDIANT\"]', 1),
(47, 'Thomine', 'Quentin', '21905900', 'M1 MIAGE', 'quentin.thomine@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$PCbm9XHnKmDkeKFi8+WrNg$iDjrOKZGRFb0pGeqTnKlJyDG8FH719pIoM1fw8X4290', '[\"ROLE_ETUDIANT\"]', 1),
(48, 'Vantomme', 'Romain', '21601313', 'M2 MIAGE - OSIE', 'romain.vantomme@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$4acu44NO71Ivi8h1Ql2EKw$xO/t+zG/HGPF9z7wyt7mGsZJBb43Kfl9wub9AHKZy4U', '[\"ROLE_ETUDIANT\"]', 1),
(49, 'Maurice', 'Samuel', '21601404', 'M2 MIAGE - OSIE', 'samuel.maurice@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$jJyy9S1jIMkK4zd1gQr8Ow$CqNLsoKyux0OuvMQ5oDyO9+BUEJsrAH0iPo3gCY2dK0', '[\"ROLE_ETUDIANT\"]', 1),
(50, 'Abel', 'Josselin', '21601636', 'M2 MIAGE - SIO', 'josselin.abel@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$/g6Mt+K8QK7dObMLiV7eYg$Pg19ZVIhpDO1DH5sdquuEplDu5IsailVs8MUHEkKZcU', '[\"ROLE_ETUDIANT\"]', 1),
(51, 'Bellot', 'Amadou', '21605539', 'M2 MIAGE - OSIE', 'amadou.bellot@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$dQ8V9NN8RFtXK/mOUNc5Cw$9rCklPpvIv3UURWQaYZ8eWosiMnnfvZGHUdetQlCAqU', '[\"ROLE_ETUDIANT\"]', 1),
(52, 'Leclère', 'Valentin', '21604916', 'M2 MIAGE - SIO', 'valentin.leclere@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$BJEugTXwHZeBxwYsXpt4RA$wPXOfy0xi/Obh6bU9OGPdVLj8VWyBN1xBdeqn5hKzLQ', '[\"ROLE_ETUDIANT\"]', 1),
(54, 'Maziere', 'Adeline', '21505384', 'M2 MIAGE - SIO', 'adeline.maziere@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$TbKd9VRHZDMfgRCKXZwisg$oywFD/4XdXa8nOSIm+tqtcqXgImYsv38WuYwxg8itvk', '[\"ROLE_ETUDIANT\"]', 1),
(65, 'Garcia', 'Florian', '21602185', 'M1 MIAGE', 'florian.garcia@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$9mwIlUb3z7++vpS79283tg$nCXY/4yOdbPDzccIyNVuJSdEI7Wxdul/amTtFFuxcgk', '[\"ROLE_ETUDIANT\"]', 1),
(66, 'Le Berre', 'Brendan', '21604296', 'M2 MIAGE - SIO', 'brendan.leberre@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$aJIM3ilCKToFc/I8LgXZgA$fVWMmVtg+mQ5rRfaf9xhuUDJeXCInmWD/dC3CGT4+Qc', '[\"ROLE_ETUDIANT\"]', 1),
(67, 'Tomala', 'Gaulthier', '21600047', 'M2 MIAGE - OSIE', 'gaulthier.tomala@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$JAjLE0r3C9zydNgVQdoK5Q$wBakBqvfxEjV9LUVntpZWbLmLeXR7y+EEWZkJcyXGxI', '[\"ROLE_ETUDIANT\"]', 1),
(68, 'Segard', 'Donatien', 's21510825', 'M2 MIAGE - SIO', 'donatien.segard@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$vjmvQsxv+eMM5CwtPT5e6Q$tDOVsiPM00M+rjXKeuz9IPkz6ivwdsyqQVtmqU/TOmc', '[\"ROLE_ETUDIANT\"]', 1),
(70, 'LEGER', 'Nicolas', '21705290', 'M1 MIAGE', 'nicolas.leger@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$6RzfgysS8azd7E1xYG0a8Q$RrlOobhsCJJyYbi5uLiMNwRysWA1JW+K3z8MZI/QFVw', '[\"ROLE_ETUDIANT\"]', 1),
(71, 'Miry', 'Victor', '21707364', 'M2 MIAGE - SIO', 'victor.miry@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$daMCPRVlxV3Ca6ptRt1U4Q$XEO+aHWAbGqd3RbV5EjxOKXAjSjNHfQmz/zN8ksNeFc', '[\"ROLE_ETUDIANT\"]', 1),
(72, 'KUNTUALA', 'Tolokela', '21407912', 'M2 MIAGE - OSIE', 'tolokela.kun@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$58/myPkeLoTMvO9oxYcIdQ$hVKSlOsqO0hRTLmOVgMMp6FUXKk+SQ1bAxZItHRitXU', '[\"ROLE_ETUDIANT\"]', 1),
(73, 'KUNTUALA', 'Tolokela', '21407912', 'M2 MIAGE - OSIE', 'Tolokela.kuntuala@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$Vxv0CvUlM3o2yjQ3J2+WEw$uGrISuFoRuW3bphWue5Vw/4E2PFfJpgQr/wZ4VWrIcM', '[\"ROLE_ETUDIANT\"]', 1),
(74, 'Douillet', 'Grégoire', '21602441', 'M2 MIAGE - SIO', 'gregoire.douillet@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$OSai6ZJWWld2ISk2kDpbxw$6TIvflivoGM/4UEw9ZYyjYpVOmn7USf1pEuJrO8dcmA', '[\"ROLE_ETUDIANT\"]', 1),
(75, 'Tillier', 'Samuel', 'T21601780', 'M2 MIAGE - SIO', 'samuel.tillier@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$1D6uIbPTt80G4SqCJFfw+w$SGUul7z+KjmqA4W+nnoF1/j86IbUt9aPJ0Yq+qZZ5Iw', '[\"ROLE_ETUDIANT\"]', 1),
(76, 'Volff', 'Lorris', '21605521', 'M2 MIAGE - SIO', 'lorris.volff@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$gwRJNUn23RU/EuBKXfjJmQ$1bXOqFJ6lbQP2N7kv/MFHT1SaDSX31kLR71G81wK0gQ', '[\"ROLE_ETUDIANT\"]', 1),
(77, 'Haddadou', 'Kamilia', '21605883', 'M1 MIAGE', '@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$plqg5zkDAKntdjleMrqSNA$t3n73uoHUUAJee0lnKqhJ6LU6YnUufoXH6j82cG5geI', '[\"ROLE_ETUDIANT\"]', 1),
(82, 'Thomas', 'Foch', '21808542', 'M1 ISRI', 'thomas.foch@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$nrqLwag2Gaaokr5jznJumA$EkXyQTgFd63Z8qxSl+GuRLI/Q/HwidRRDvF/b+EH+gA', '[\"ROLE_ETUDIANT\"]', 1),
(83, 'Sannier', 'Mathis', '21702743', 'M1 ISRI', '', '$argon2id$v=19$m=65536,t=4,p=1$nrqLwag2Gaaokr5jznJumA$EkXyQTgFd63Z8qxSl+GuRLI/Q/HwidRRDvF/b+EH+gA', '[\"ROLE_ETUDIANT\"]', 1),
(84, 'haniche', 'karim', '21707031', 'M2 MIAGE - OSIE', 'karim.haniche@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$IeakQCvOMu9v13K8Z2XLdw$80n+ujwNRUCXvWjh3ZXN4un6wzc2IuOsLoRjd9EU08M', '[\"ROLE_ETUDIANT\"]', 1),
(85, 'Haddadou', 'Kamilia', '21605883', 'M1 MIAGE', 'kamilia.haddadou@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$g3rfzFikbwtiGxFjq+SN1Q$C0mv5Dw64E8+z2Nyo+FwIspzPGWvjABatcRj/FXKXdk', '[\"ROLE_ETUDIANT\"]', 1),
(86, 'Savard', 'Louis', '21601689', 'M1 MIAGE', 'louis.savard@etud.u-picardie.fr', '$argon2id$v=19$m=65536,t=4,p=1$bfG9qMQDSYPdDlXT3cCvYw$DTcANx+GUn7ZfThpuAdizT3KlRYVf/SU94+ZDyyI6dE', '[\"ROLE_ETUDIANT\"]', 1);

-- --------------------------------------------------------

--
-- Table structure for table `famille`
--

CREATE TABLE `famille` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `famille`
--

INSERT INTO `famille` (`id`, `nom`, `description`) VALUES
(4, 'Ordinateur portable Windows', 'Dell, HP'),
(5, 'Ordinateur portable Apple', 'Macbook Pro/Air'),
(6, 'Kit ASE', 'Kit de développement'),
(7, 'Kit IoT', 'Développement objets connectés'),
(8, 'Kit avion', 'Kit composé d\'un planeur à destination de l\'UE découverte de la recherche'),
(9, 'Tablette Ipad', 'Ipad de base');

-- --------------------------------------------------------

--
-- Table structure for table `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20200304091725', '2020-04-21 14:47:39');

-- --------------------------------------------------------

--
-- Table structure for table `relation_role_utilisateur`
--

CREATE TABLE `relation_role_utilisateur` (
  `id` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `id_utilisateur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `role_Name` varchar(32) NOT NULL,
  `role_Desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `role_Name`, `role_Desc`) VALUES
(1, 'Secrétariat', 'Description du rôle secrétariat');

-- --------------------------------------------------------

--
-- Table structure for table `statut_dossier`
--

CREATE TABLE `statut_dossier` (
  `id` int(11) NOT NULL,
  `statut_nom` varchar(32) NOT NULL,
  `statut_desc` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Contient l''ensemble des statuts des dossiers';

--
-- Dumping data for table `statut_dossier`
--

INSERT INTO `statut_dossier` (`id`, `statut_nom`, `statut_desc`) VALUES
(1, 'Déposé- Attente accord scolarité', 'Le dossier a été déposé. Il est en attente de validation par la scolarité'),
(2, 'OK scola - attente responsable', 'Le dossier a été validé par le secrétariat. Il est en attente de validation par le responsable pédagogique.'),
(3, 'rejet secrétariat', 'Le dossier a été rejeté par le secrétariat. Consultez-le pour voir les raisons. Corrigez les problèmes et refaites un dossier.'),
(4, 'validé responsable', 'Le dossier a été validé par le responsable pédagogique'),
(5, 'rejet responsable', 'Le dossier a été rejeté par le responsable pédagogique. Corrigez les problèmes et refaites un dossier.'),
(6, 'matériel prêt à disposition', 'Dossier validé. En attente de récupération du matériel par l\'étudiant.'),
(7, 'en prêt', 'Matériel en possession de l\'étudiant'),
(8, 'pret clos', 'le matériel a été rendu'),
(9, 'modification', 'l\'étudiant a modifié sa demande');

-- --------------------------------------------------------

--
-- Table structure for table `statut_materiel`
--

CREATE TABLE `statut_materiel` (
  `id` int(11) NOT NULL,
  `statut_nom` varchar(32) NOT NULL,
  `statut_desc` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `statut_materiel`
--

INSERT INTO `statut_materiel` (`id`, `statut_nom`, `statut_desc`) VALUES
(1, 'en prêt', 'le matériel a été prêté à un édudiant'),
(2, 'disponible', NULL),
(3, 'en réparation', 'le matériel n\'est pas disponible pour le prêt'),
(4, 'perdu', NULL),
(5, 'Sorti d\'inventaire', 'Matériel obsolète mis au rebut');

-- --------------------------------------------------------

--
-- Table structure for table `token`
--

CREATE TABLE `token` (
  `id` int(11) NOT NULL,
  `hash` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `type_materiel`
--

CREATE TABLE `type_materiel` (
  `id` int(11) NOT NULL,
  `id_famille` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `type_materiel`
--

INSERT INTO `type_materiel` (`id`, `id_famille`, `nom`, `description`) VALUES
(1, 4, 'Dell', '13 pouces'),
(2, 4, 'Dell', '15 pouces'),
(4, 6, 'Rasberry 2018', 'dernier rasberry acheté'),
(5, 7, 'Arduino', 'Arduino + capteur'),
(6, 4, 'ASUS 15 pouces', NULL),
(7, 4, 'HP', 'ordinateur portable hp'),
(8, 5, 'MacBookPro', 'MacBookPro'),
(9, 6, 'Kits ASE', 'Ensemble ASE');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `roles`, `password`) VALUES
(20, 'anne.lapujade@u-picardie.fr', '[\"ROLE_ADMIN\"]', '$argon2id$v=19$m=65536,t=4,p=1$0WGsKSldJLSfuZWOP82vvA$2avl2N7I9K+4dLOLrslgDHE/qqS+/dReJVtKhCSbDHU'),
(22, 'miage@u-picardie.fr', '[\"ROLE_SECRETARIAT_MIAGE\", \"ROLE_ADMIN\", \"ROLE_RESPONSABLE\"]', '$argon2id$v=19$m=65536,t=4,p=1$4DSjRWCk2O4B93esWXZbGQ$+APhV6cdrT3NpitTdhfwGT2AcyLi/0b6VylI5VKnwEk'),
(23, 'pascal.vaniet@u-picardie.fr', '[\"ROLE_INFORMATIQUE\", \"ROLE_ADMIN\"]', '$argon2id$v=19$m=65536,t=4,p=1$0WGsKSldJLSfuZWOP82vvA$2avl2N7I9K+4dLOLrslgDHE/qqS+/dReJVtKhCSbDHU'),
(24, 'florentin.pauchet@u-picardie.fr', '[\"ROLE_INFORMATIQUE\"]', '$argon2id$v=19$m=65536,t=4,p=1$0WGsKSldJLSfuZWOP82vvA$2avl2N7I9K+4dLOLrslgDHE/qqS+/dReJVtKhCSbDHU'),
(26, 'anne-marie.fontaine@u-picardie.fr', '[\"ROLE_SECRETARIAT_ISRI\", \"ROLE_ADMIN\"]', '$argon2id$v=19$m=65536,t=4,p=1$0WGsKSldJLSfuZWOP82vvA$2avl2N7I9K+4dLOLrslgDHE/qqS+/dReJVtKhCSbDHU');

-- --------------------------------------------------------

--
-- Table structure for table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id` int(11) NOT NULL,
  `identifiant` varchar(32) NOT NULL,
  `mot_de_passe` varchar(32) NOT NULL,
  `mail` text NOT NULL,
  `prenom` varchar(32) NOT NULL,
  `nom` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `identifiant`, `mot_de_passe`, `mail`, `prenom`, `nom`) VALUES
(1, 'mg', 'admin', 'martin@email.com', 'Martin', 'GT');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acces_materiel`
--
ALTER TABLE `acces_materiel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dossier_pret`
--
ALTER TABLE `dossier_pret`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name_materiel` (`id_equipement`),
  ADD KEY `contrainte_duree_pret` (`id_duree_pret`),
  ADD KEY `contrainte_id_statut_dossier` (`id_statut`),
  ADD KEY `contrainte_id_famille` (`id_famille`),
  ADD KEY `contrainte_id_connexion` (`id_etudiant`);

--
-- Indexes for table `duree_pret`
--
ALTER TABLE `duree_pret`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `equipement`
--
ALTER TABLE `equipement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name_materiel` (`id_type_materiel`),
  ADD KEY `contrainte_acces_materiel` (`id_acces_materiel`),
  ADD KEY `contrainte_statut_materiel` (`id_statut_materiel`);

--
-- Indexes for table `etudiant`
--
ALTER TABLE `etudiant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `famille`
--
ALTER TABLE `famille`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `relation_role_utilisateur`
--
ALTER TABLE `relation_role_utilisateur`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contrainte_role` (`id_role`),
  ADD KEY `contrainte_utilisateur` (`id_utilisateur`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statut_dossier`
--
ALTER TABLE `statut_dossier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statut_materiel`
--
ALTER TABLE `statut_materiel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `token`
--
ALTER TABLE `token`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type_materiel`
--
ALTER TABLE `type_materiel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contrainte_famille` (`id_famille`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`);

--
-- Indexes for table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acces_materiel`
--
ALTER TABLE `acces_materiel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `dossier_pret`
--
ALTER TABLE `dossier_pret`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `duree_pret`
--
ALTER TABLE `duree_pret`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `equipement`
--
ALTER TABLE `equipement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;

--
-- AUTO_INCREMENT for table `etudiant`
--
ALTER TABLE `etudiant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `famille`
--
ALTER TABLE `famille`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `relation_role_utilisateur`
--
ALTER TABLE `relation_role_utilisateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `statut_dossier`
--
ALTER TABLE `statut_dossier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `statut_materiel`
--
ALTER TABLE `statut_materiel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `token`
--
ALTER TABLE `token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `type_materiel`
--
ALTER TABLE `type_materiel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `dossier_pret`
--
ALTER TABLE `dossier_pret`
  ADD CONSTRAINT `contrainte_duree_pret` FOREIGN KEY (`id_duree_pret`) REFERENCES `duree_pret` (`id`),
  ADD CONSTRAINT `contrainte_id_connexion` FOREIGN KEY (`id_etudiant`) REFERENCES `etudiant` (`id`),
  ADD CONSTRAINT `contrainte_id_equipement` FOREIGN KEY (`id_equipement`) REFERENCES `equipement` (`id`),
  ADD CONSTRAINT `contrainte_id_famille` FOREIGN KEY (`id_famille`) REFERENCES `famille` (`id`),
  ADD CONSTRAINT `contrainte_id_statut_dossier` FOREIGN KEY (`id_statut`) REFERENCES `statut_dossier` (`id`);

--
-- Constraints for table `equipement`
--
ALTER TABLE `equipement`
  ADD CONSTRAINT `contrainte_acces_materiel` FOREIGN KEY (`id_acces_materiel`) REFERENCES `acces_materiel` (`id`),
  ADD CONSTRAINT `contrainte_statut_materiel` FOREIGN KEY (`id_statut_materiel`) REFERENCES `statut_materiel` (`id`),
  ADD CONSTRAINT `contrainte_type_materiel` FOREIGN KEY (`id_type_materiel`) REFERENCES `type_materiel` (`id`);

--
-- Constraints for table `relation_role_utilisateur`
--
ALTER TABLE `relation_role_utilisateur`
  ADD CONSTRAINT `contrainte_role` FOREIGN KEY (`id_role`) REFERENCES `role` (`id`),
  ADD CONSTRAINT `contrainte_utilisateur` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id`);

--
-- Constraints for table `type_materiel`
--
ALTER TABLE `type_materiel`
  ADD CONSTRAINT `contrainte_famille` FOREIGN KEY (`id_famille`) REFERENCES `famille` (`id`);
