<html>
<head>
<style>
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
</style>
</head>
<body>
<?php
	  $host_name = 'db5000351249.hosting-data.io';
      $database = 'dbs341443';
      $user_name = 'dbu358342';
      $password = 'Miage2020%';
  
  $mysqli = new mysqli($host_name, $user_name, $password, $database);

  // Oh non ! Une connect_errno existe donc la tentative de connexion a échoué !
  if ($mysqli->connect_errno) {
      // La connexion a échoué. Que voulez-vous faire ? 
      // Vous pourriez vous contacter (email ?), enregistrer l'erreur, afficher une jolie page, etc.
      // Vous ne voulez pas révéler des informations sensibles
  
      // Essayons ceci :
      echo "Désolé, le site web subit des problèmes.";
  
      // Quelque chose que vous ne devriez pas faire sur un site public,
      // mais cette exemple vous montrera quand même comment afficher des
      // informations lié à l'erreur MySQL -- vous voulez peut être enregistrer ceci
      echo "Error: Échec d'établir une connexion MySQL, voici pourquoi : \n";
      echo "Errno: " . $mysqli->connect_errno . "\n";
      echo "Error: " . $mysqli->connect_error . "\n";
      
      // Vous voulez peut être leurs afficher quelque chose de jolie, nous ferons simplement un exit
      exit;
  }
  
  // Réaliser une requête SQL
  //$sql = "SELECT * FROM etudiant";

 $sql = "SELECT dossier_pret.id,
        etudiant.formation as formation, 
        etudiant.numero as numeroEtudiant, 
        etudiant.nom as nom, 
        etudiant.prenom as prenom,
        duree_pret.nom as dureePret, 
        type_materiel.nom as typeMateriel,
        equipement.code_barre as codeBarre, 
        famille.nom as famille, 
        statut_dossier.statut_nom as statutDossier
  FROM `etudiant`, dossier_pret,equipement, famille, duree_pret, type_materiel, statut_dossier 
  WHERE
  etudiant.id=dossier_pret.id_etudiant AND
  dossier_pret.id_equipement=equipement.id AND
  dossier_pret.id_duree_pret=duree_pret.id AND
  equipement.id_type_materiel=type_materiel.id AND
  statut_dossier.id=dossier_pret.id_statut AND
  type_materiel.id_famille=famille.id
  ORDER BY statut_dossier.statut_nom ASC, etudiant.nom ASC";


  if (!$result = $mysqli->query($sql)) {
      // Oh non ! La requête a échoué. 
      echo "Désolé, le site web subit des problèmes.";
  
      // Denouveau, ne faite pas ceci sur un site public, mais nous vous
      // montrerons comment récupérer les informations de l'erreur
      echo "Error: Notre requête a échoué lors de l'exécution et voici pourquoi :\n";
      echo "Query: " . $sql . "\n";
      echo "Errno: " . $mysqli->errno . "\n";
      echo "Error: " . $mysqli->error . "\n";
      exit;
  }
  
  // Phew, nous avons réussi. Nous savons que la connexion MySQL et la requête 
  // a réussi, mais avons nous un résultat ?
  if ($result->num_rows === 0) {
      // Oh, pas de lignes ! Dès fois c'est acceptable et attendue, dès fois
      // ce l'est pas. Vous décidez. Dans ce cas, peut être que actor_id était trop
      // large ?
      echo "Requête vide.";
      exit;
  }
  
  echo "<table >";
  echo "<tr>";
  echo "<th>Statut dossier</th>";
  echo "<th>Numéro étudiant</th>";
  echo "<th>Nom Etudiant</th>";
  echo "<th>Prenom Etudiant</th>";
  echo "<th>Formation Etudiant</th>";
  echo "<th>Durée prêt</th>";
  echo "<th>Famille matériel</th>";
  echo "<th>Type matériel</th>";
  echo "<th>Code barre</th>";
  echo "</tr>";
  while ($etudiant = $result->fetch_assoc()) {
    echo "<tr>";
    echo "<td>".$etudiant['statutDossier']."</td>";
    echo "<td>".$etudiant['numeroEtudiant']."</td>";
    echo "<td>".$etudiant['nom']."</td>";
    echo "<td>".$etudiant['prenom']."</td>";
    echo "<td>".$etudiant['formation']."</td>";
    echo "<td>".$etudiant['dureePret']."</td>";
    echo "<td>".$etudiant['famille']."</td>";
    echo "<td>".$etudiant['typeMateriel']."</td>";
    echo "<td>".$etudiant['codeBarre']."</td>";

    echo "</tr>";
  }
  echo "</table>";
  
  // Le script libérera automatiquement le résultat et fermera la connexion
  // MySQL quand elle existe, mais faisons le quand même
  $result->free();
  $mysqli->close();
?>
</body>
</html>