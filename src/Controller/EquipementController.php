<?php

namespace App\Controller;

use App\Entity\AccesMateriel;
use App\Entity\DossierPret;
use App\Entity\Equipement;
use App\Entity\StatutMateriel;
use App\Form\EquipementType;
use App\Entity\TypeMateriel;
use App\Entity\Famille;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/equipement")
 */
class EquipementController extends AbstractController
{
    /**
     * @Route("/", name="equipement_index", methods={"GET"})
     */
    public function index(): Response
    {
        $equipements = $this->getDoctrine()
            ->getRepository(Equipement::class)
            ->findAll();

        return $this->render('equipement/index.html.twig', [
            'equipements' => $equipements,
        ]);
    }

    /**
     * @Route("/new", name="equipement_new", methods={"GET","POST"})
     */
    
    public function new(Request $request): Response
    {
        $equipement = new Equipement();
        $form = $this->createForm(EquipementType::class, $equipement);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $default_statut = $this->getDoctrine()
                ->getRepository(StatutMateriel::class)
                ->find(2);
            $equipement->setIdStatutMateriel($default_statut);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($equipement);
            $entityManager->flush();

            return $this->redirectToRoute('dossier_pret_dashboard', array("page"=>"equipement"));
        }

        return $this->render('equipement/new.html.twig', [
            'equipement' => $equipement,
            'form' => $form->createView(),
        ]);
    }

    

    /**
     * @Route("/{id}", name="equipement_show", methods={"GET"})
     */
    public function show(Equipement $equipement): Response
    {
        return $this->render('equipement/show.html.twig', [
            'equipement' => $equipement,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="equipement_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Equipement $equipement): Response
    {
        $form = $this->createForm(EquipementType::class, $equipement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('dossier_pret_dashboard', array("page"=>"equipement"));
        }

        return $this->render('equipement/edit.html.twig', [
            'equipement' => $equipement,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="equipement_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Equipement $equipement): Response
    {
        $this->get('session')->getFlashBag()->get('warning');
        $check = null;
        if ($this->isCsrfTokenValid('delete'.$equipement->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $check = $entityManager->getRepository(DossierPret::class)->findByIdEquipement($equipement);
            if($check == null) {
                $entityManager->remove($equipement);
                $entityManager->flush();
            }else{
                $this->addFlash('warning', 'Erreur: il existe un dossier de prêt en cours avec cet équipement.');
                return $this->redirectToRoute('equipement_edit', ["id" => $equipement->getId()]);
            }


        }

        return $this->redirectToRoute('dossier_pret_dashboard', array("page"=>"equipement"));

    }


    /**
     * @Route("/informatique/inventaire", name="statut_materiel_inventaire", methods={"GET"})
     */
    public function inventaire(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $sql = 'SELECT tt1.Type_du_materiel, tt1.Total,
        CASE WHEN tt2.En_Stock > 0 THEN tt2.En_Stock ELSE 0 END AS En_Stock,
        CASE WHEN tt5.En_Pret > 0 THEN tt5.En_Pret ELSE 0 END AS En_Pret
        FROM (SELECT CONCAT(nom, \' - \' , description) AS Type_du_materiel, count(*) as Total
              FROM type_materiel as t1
              INNER JOIN equipement AS t2 ON t2.id_type_materiel = t1.id
              GROUP BY t2.id_type_materiel) as tt1

        LEFT JOIN (SELECT CONCAT(t1.nom, \' - \' , t1.description) AS Type_du_materiel, COUNT(*) as En_Stock
                   FROM type_materiel AS t1
                   LEFT JOIN equipement AS t2 ON t2.id_type_materiel = t1.id
                   WHERE t2.id_statut_materiel = 2
                   GROUP BY Type_du_materiel) as tt2 on tt1.Type_du_materiel = tt2.Type_du_materiel

        LEFT JOIN (SELECT CONCAT(t1.nom, \' - \' , t1.description) AS Type_du_materiel, COUNT(*) as En_Pret
           FROM type_materiel AS t1
           LEFT JOIN equipement AS t2 ON t2.id_type_materiel = t1.id
           WHERE t2.id_statut_materiel = 1
           GROUP BY Type_du_materiel) as tt5 on tt1.Type_du_materiel = tt5.Type_du_materiel';

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();

        return $this->render('equipement/inventaire.html.twig', [
            'equipements' => $result,
        ]);

        return new Response(
            '<html><body>Lucky number: '.var_dump($result).'</body></html>'
        );
    }


}
