<?php

namespace App\Controller;

use App\Entity\DossierPret;
use App\Entity\DureePret;
use App\Form\DureePretType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;


/**
 * @Route("/duree/pret")
 */
class DureePretController extends AbstractController
{
    /**
     * @Route("/", name="duree_pret_index", methods={"GET"})
     */
    public function index(): Response
    {
        $dureePrets = $this->getDoctrine()
            ->getRepository(DureePret::class)
            ->findAll();

        return $this->render('duree_pret/index.html.twig', [
            'duree_prets' => $dureePrets,
        ]);
    }

    /**
     * @Route("/new", name="duree_pret_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $dureePret = new DureePret();
        $form = $this->createForm(DureePretType::class, $dureePret);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($dureePret);
            $entityManager->flush();

            return $this->redirectToRoute('duree_pret_index');
        }

        return $this->render('duree_pret/new.html.twig', [
            'duree_pret' => $dureePret,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="duree_pret_show", methods={"GET"})
     */
    public function show(DureePret $dureePret): Response
    {
        return $this->render('duree_pret/show.html.twig', [
            'duree_pret' => $dureePret,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="duree_pret_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, DureePret $dureePret): Response
    {
        $form = $this->createForm(DureePretType::class, $dureePret);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('duree_pret_index');
        }

        return $this->render('duree_pret/edit.html.twig', [
            'duree_pret' => $dureePret,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="duree_pret_delete", methods={"DELETE"})
     */
    public function delete(Request $request, DureePret $dureePret): Response
    {

        $this->get('session')->getFlashBag()->get('warning');
        $check = null;
        if ($this->isCsrfTokenValid('delete'.$dureePret->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $check = $entityManager->getRepository(DossierPret::class)->findByIdDureePret($dureePret);
            if($check == null) {
                $entityManager->remove($dureePret);
                $entityManager->flush();
            }else{
                $this->addFlash('warning', 'Erreur: il existe un ou plusieurs équipements avec ce statut');
                return $this->redirectToRoute('duree_pret_edit', ["id" => $dureePret->getId()]);
            }
        }

        return $this->redirectToRoute('duree_pret_index');
    }


    /**
     * @Route("/get/list", name="duree_pret_list", methods={"GET"})
     */
    public function list() {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $sql = 'SELECT * from duree_pret';
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return new JsonResponse($result);
    }

    
}
