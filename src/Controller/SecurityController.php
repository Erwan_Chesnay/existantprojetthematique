<?php

namespace App\Controller;

use App\Entity\Etudiant;
use App\Entity\User;
use App\Form\ResetPassword;
use App\Security\LoginFormAuthenticator;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    public function redirectSecurity(){
        return $this->render('security/choice.html.twig');
    }

    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout", methods={"GET"})
     */
    public function logout()
    {
        // controller can be blank: it will never be executed!
        throw new \Exception('Don\'t forget to activate logout in security.yaml');
    }

    /**
     * @Route("/reset", name="app_reset")
     */
    public function reset(Request $request, UserPasswordEncoderInterface $passwordEncoder, GuardAuthenticatorHandler $guardHandler, LoginFormAuthenticator $authenticator)
    {
        $etudiant = $this->getUser();
        $form = $this->createFormBuilder()
            ->add('password', PasswordType::class, ['required' => true,])
            ->add('confirm', PasswordType::class, ['required' => true,])
            ->add('save', SubmitType::class, ['label' => 'Envoyer'])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $etudiant->setPassword(
                $passwordEncoder->encodePassword(
                    $etudiant,
                    $form->get('password')->getData()
                )
            );
            $entityManager->persist($etudiant);
            $entityManager->flush();
            return $this->redirectToRoute('app_logout');
        }

        return $this->render('security/changePassword.html.twig', [
            'form' => $form->createView(),
        ]);


    }



    /**
     * @Route("/getreset", name="app_get_reset", methods={"GET","POST"})
     */
    public function getReset(Request $request, UserPasswordEncoderInterface $passwordEncoder, GuardAuthenticatorHandler $guardHandler, LoginFormAuthenticator $authenticator, MailerInterface $mailer){ //demande du lien
        $etudiant = new Etudiant();
        $form = $this->createFormBuilder($etudiant)
            ->add('email', TextType::class)
            ->add('save', SubmitType::class, ['label' => 'Envoyer'])
            ->getForm();

        

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $etudiant = $entityManager->getRepository(Etudiant::class)->findOneByEmail($form->get('email')->getData());

            if($etudiant === null){
                return $this->render('security/reset.html.twig', [
                    'form' => $form->createView(),
                ]);
            }

            $length = 8;
            $chars =  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.
                '0123456789`-=~!@#$%^&*()_+,./<>?;:[]{}\|';

            $plainPassword = '';
            $max = strlen($chars) - 1;

            for ($i=0; $i < $length; $i++) {
                $plainPassword .= $chars[random_int(0, $max)];
            }
            

            if($etudiant === null){
                return $this->render('security/login.html.twig', [
                    'form' => $form->createView(),
                ]);
            }else{
                $etudiant->setPassword(
                    $passwordEncoder->encodePassword(
                        $etudiant,
                        $plainPassword
                    )
                );


                $entityManager->persist($etudiant);
                $entityManager->flush();
                $this->mailToEtudiant($mailer, $etudiant, $plainPassword);
            }


            return $this->redirectToRoute('app_login');

        }


        return $this->render('security/reset.html.twig', [
            'form' => $form->createView(),
        ]);
    }




    private function mailToEtudiant(MailerInterface $mailer, Etudiant $etudiant, String $mdp) //ACCEPTER PAR LE SECRETARIAT
    {

        $email = (new Email())
            ->from('no-reply@pret.miage-amiens.fr')
            ->to($etudiant->getEmail()) //ETUDIANT
            ->subject('[ApplicationPrêt] - Mot de passe oublié')
            ->text('Votre passe temporaire est: '.$mdp.', merci de le changer dès la première connexion.')

        ;
        try {
            $mailer->send($email);
        } catch (TransportExceptionInterface $e) {
        }
    }
}
