<?php

namespace App\Controller;

use App\Entity\StatutDossier;
use App\Form\StatutDossierType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/statut/dossier")
 */
class StatutDossierController extends AbstractController
{
    /**
     * @Route("/", name="statut_dossier_index", methods={"GET"})
     */
    public function index(): Response
    {
        $statutDossiers = $this->getDoctrine()
            ->getRepository(StatutDossier::class)
            ->findAll();

        return $this->render('statut_dossier/index.html.twig', [
            'statut_dossiers' => $statutDossiers,
        ]);
    }

    /**
     * @Route("/new", name="statut_dossier_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $statutDossier = new StatutDossier();
        $form = $this->createForm(StatutDossierType::class, $statutDossier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($statutDossier);
            $entityManager->flush();

            return $this->redirectToRoute('dossier_pret_dashboard_administration', array("page"=>"statut_dossier"));
        }

        return $this->render('statut_dossier/new.html.twig', [
            'statut_dossier' => $statutDossier,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="statut_dossier_show", methods={"GET"})
     */
    public function show(StatutDossier $statutDossier): Response
    {
        return $this->render('statut_dossier/show.html.twig', [
            'statut_dossier' => $statutDossier,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="statut_dossier_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, StatutDossier $statutDossier): Response
    {
        $form = $this->createForm(StatutDossierType::class, $statutDossier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('dossier_pret_dashboard_administration', array("page"=>"statut_dossier"));
        }

        return $this->render('statut_dossier/edit.html.twig', [
            'statut_dossier' => $statutDossier,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="statut_dossier_delete", methods={"DELETE"})
     */
    public function delete(Request $request, StatutDossier $statutDossier): Response
    {
        if ($this->isCsrfTokenValid('delete'.$statutDossier->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($statutDossier);
            $entityManager->flush();
        }

        return $this->redirectToRoute('dossier_pret_dashboard_administration', array("page"=>"statut_dossier"));
    }
}
