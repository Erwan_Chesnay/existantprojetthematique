<?php

namespace App\Controller;

use App\Entity\AccesMateriel;
use App\Form\AccesMaterielType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/acces/materiel")
 */
class AccesMaterielController extends AbstractController
{
    /**
     * @Route("/", name="acces_materiel_index", methods={"GET"})
     */
    public function index(): Response
    {
        $accesMateriels = $this->getDoctrine()
            ->getRepository(AccesMateriel::class)
            ->findAll();

        return $this->render('acces_materiel/index.html.twig', [
            'acces_materiels' => $accesMateriels,
        ]);
    }

    /**
     * @Route("/new", name="acces_materiel_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $accesMateriel = new AccesMateriel();
        $form = $this->createForm(AccesMaterielType::class, $accesMateriel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($accesMateriel);
            $entityManager->flush();

            return $this->redirectToRoute('acces_materiel_index');
        }

        return $this->render('acces_materiel/new.html.twig', [
            'acces_materiel' => $accesMateriel,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="acces_materiel_show", methods={"GET"})
     */
    public function show(AccesMateriel $accesMateriel): Response
    {
        return $this->render('acces_materiel/show.html.twig', [
            'acces_materiel' => $accesMateriel,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="acces_materiel_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, AccesMateriel $accesMateriel): Response
    {
        $form = $this->createForm(AccesMaterielType::class, $accesMateriel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('acces_materiel_index');
        }

        return $this->render('acces_materiel/edit.html.twig', [
            'acces_materiel' => $accesMateriel,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="acces_materiel_delete", methods={"DELETE"})
     */
    public function delete(Request $request, AccesMateriel $accesMateriel): Response
    {
        if ($this->isCsrfTokenValid('delete'.$accesMateriel->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($accesMateriel);
            $entityManager->flush();
        }

        return $this->redirectToRoute('acces_materiel_index');
    }
}
