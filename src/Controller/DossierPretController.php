<?php

namespace App\Controller;

use App\Entity\DossierPret;
use App\Entity\DureePret;
use App\Entity\Etudiant;
use App\Entity\Famille;
use App\Entity\Professeur;
use App\Entity\StatutDossier;
use App\Entity\Equipement;
use App\Entity\User;
use App\Form\DossierPretGroupeType;
use App\Form\DossierPretType;
use App\Form\DossierPretTypeEdit;
use App\Form\DossierPretGroupeTypeEdit;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Entity\TypeMateriel;
use App\Entity\StatutMateriel;
use Doctrine\ORM\Query;

use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

use Knp\Snappy\Pdf;


/**
 * @Route("/dossier/pret")
 */
class DossierPretController extends AbstractController
{
    private $manager;
    private $security;


    public function __construct(Pdf $manager, Security $security)
    {
        $this->security = $security;
        $this->manager = $manager;
    }




    /**
     * @Route("/", name="dossier_pret_index", methods={"GET"})
     */
    public function index(): Response
    {
        $dossierPrets = $this->getDoctrine()
            ->getRepository(DossierPret::class)
            ->findAll();

        return $this->render('dossier_pret/index.html.twig', [
            'dossier_prets' => $dossierPrets,
        ]);
    }



    /**
     * @Route("/secretariat", name="dossier_pret_secretariat", methods={"GET"})
     */
    public function secretariat(): Response
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (in_array('ROLE_ADMIN', $user->getRoles())) {
            $result = $this->getDoctrine()
                ->getRepository(DossierPret::class)
                ->findBySecretariat();
        } else if(in_array('ROLE_SECRETARIAT_MIAGE', $user->getRoles())){
            $result = $this->getDoctrine()
                ->getRepository(DossierPret::class)
                ->findBySecretariatMiage();
        }else if(in_array('ROLE_SECRETARIAT_ISRI', $user->getRoles())){
            $result = $this->getDoctrine()
                ->getRepository(DossierPret::class)
                ->findBySecretariatIsri();
        }

        return $this->render('dossier_pret/secretariat.html.twig', [
            'dossier_prets' => $result,
        ]);
    }

    /**
     * @Route("/secretariat/valide/{id}", name="dossier_pret_valide", methods={"GET"})
     */
    public function valide(DossierPret $dossierPret, MailerInterface $mailer): Response //VALIDATION DE LA DEMANDE PAR LE SECRETARIAT
    {
        $statut = $this->getDoctrine()
            ->getRepository(StatutDossier::class)->find(2);


        $dossierPret->setIdStatut($statut);
        $em = $this->getDoctrine()->getManager();
        $em->persist($dossierPret);
        $em->flush();

        $this->mailToResponsable($mailer, $dossierPret);

        return $this->redirectToRoute('dossier_pret_secretariat');

    }

    /**
     * @Route("/rejet/{id}", name="dossier_pret_rejete", methods={"GET"})
     */
    public function rejet(DossierPret $dossierPret): Response
    {
        $statut = $this->getDoctrine()->getRepository(StatutDossier::class)->find(3);
        $dossierPret->setIdStatut($statut);

        $em = $this->getDoctrine()->getManager();
        $em->persist($dossierPret);
        $em->flush();



        return $this->redirectToRoute('dossier_pret_secretariat');
    }

    /**
     * @Route("/responsable", name="dossier_pret_responsable", methods={"GET"})
     */
    public function responsable(): Response
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $persoUPJV = $result = $this->getDoctrine()
            ->getRepository(DossierPret::class)
            ->findByResponsablePersoUPJV();
        if (in_array('ROLE_ADMIN', $user->getRoles())) {
            $persoUPJV = null;
            $result = $this->getDoctrine()
                ->getRepository(DossierPret::class)
                ->findByResponsable();
        } else if (in_array('ROLE_RESPONSABLE_MIAGE', $user->getRoles())){
            $result = $this->getDoctrine()
                ->getRepository(DossierPret::class)
                ->findByResponsableMIAGE();
        } else if (in_array('ROLE_RESPONSABLE_ISRI', $user->getRoles())){
            $result = $this->getDoctrine()
                ->getRepository(DossierPret::class)
                ->findByResponsableISRI();
        }

        if (!empty($persoUPJV) && !empty($result)) {
            $result = array_merge($result, $persoUPJV);
        }

        return $this->render('dossier_pret/responsable.html.twig', [
            'dossier_prets' => $result,
        ]);
    }

    /**
     * @Route("/valideResponsable/{id}", name="dossier_pret_valide_responsable", methods={"GET"})
     */
    public function valideResponsable(DossierPret $dossierPret): Response
    {
        $statut = $this->getDoctrine()
            ->getRepository(StatutDossier::class)->find(4);


        $dossierPret->setIdStatut($statut);
        $em = $this->getDoctrine()->getManager();
        $em->persist($dossierPret);
        $em->flush();

        return $this->redirectToRoute('dossier_pret_responsable');

    }

    /**
     * @Route("/rejetResponsable/{id}", name="dossier_pret_rejete_responsable", methods={"GET"})
     */
    public function rejetResponsable(DossierPret $dossierPret): Response
    {
        $statut = $this->getDoctrine()
            ->getRepository(StatutDossier::class)->find(5);
        $dossierPret->setIdStatut($statut);

        $em = $this->getDoctrine()->getManager();
        $em->persist($dossierPret);
        $em->flush();

        return $this->redirectToRoute('dossier_pret_responsable');
    }


    
    public function nomPDF(Object $nom, String $type): String{
        $originalFilename = pathinfo($nom->getClientOriginalName(), PATHINFO_FILENAME);
        // this is needed to safely include the file name as part of the URL
        $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
        $newFilename = $safeFilename.'-'.uniqid().'.'.$nom->guessExtension();

        // Move the file to the directory where brochures are stored
        try {
            if($type == "attestation"){
                $nom->move($this->getParameter('attestation_directory'),$newFilename);    
            }else{
                $nom->move($this->getParameter('carte_directory'),$newFilename);
            }
            
        } catch (FileException $e) {
            // ... handle exception if something happens during file upload
            echo "error";
        }
        return $newFilename;
    }

    /**
     * @Route("/new", name="dossier_pret_new", methods={"GET","POST"})
     */
    public function new(Request $request, MailerInterface $mailer): Response
    {
        $user = $this->security->getUser();
        $dossierPret = new DossierPret();
        $form = $this->createForm(DossierPretType::class, $dossierPret);
        $form->handleRequest($request);

        $repoD = $this->getDoctrine()->getRepository(DossierPret::class);
        $familles = $repoD->findByFamilleDispo();

        if ($form->isSubmitted() && $form->isValid()) {

            $repoStatut = $this->getDoctrine()->getRepository(StatutDossier::class);
            if(in_array('ROLE_ETUDIANT', $user->getRoles())) {
                $dossierPret->setAttestationAssurance1($this->nomPDF($form->get('attestationAssurance1')->getData(), "attestation"));
                $dossierPret->setCarteEtudiant1($this->nomPDF($form->get('carteEtudiant1')->getData(), "carte"));
                $statut = $repoStatut->find(1);
            }
            $idFamille = $request->request->get('familles');

            if(in_array('ROLE_PROFESSEUR', $user->getRoles())) {
                $repoProfesseur = $this->getDoctrine()->getRepository(Professeur::class);
                $dossierPret->setIdProfesseur($repoProfesseur->find($user->getId()));
                $dossierPret->setFormation('Personnel UPJV');
                $statut = $repoStatut->find(2);
            } else {
                $repoEtudiant = $this->getDoctrine()->getRepository(Etudiant::class);
                $dossierPret->setIdEtudiant($repoEtudiant->find($user->getId()));
            }
            $dossierPret->setIdStatut($statut);

            $dossierPret->setIdDureePret(null);
            $dossierPret->setGroupe(false);
            $repoFamille = $this->getDoctrine()->getRepository(Famille::class);
            $famille = $repoFamille->find($idFamille);
            $dossierPret->setIdFamille($famille);

            $date = date("d/m/Y");
            $dossierPret->setDateDemande(\DateTime::createFromFormat('d/m/Y', $date));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($dossierPret);
            $entityManager->flush();

            $this->mailToSecretariat($mailer, $dossierPret);
            return $this->redirectToRoute('dossier_pret_mon_dossier'); // A CHANGER
        }

        return $this->render('dossier_pret/new.html.twig', [
            'dossier_pret' => $dossierPret,
            'familles' => $familles,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/newGroupe", name="dossier_pret_new_groupe", methods={"GET","POST"})
     */
    public function newGroupe(Request $request): Response
    {
        $dossierPret = new DossierPret();
        $form = $this->createForm(DossierPretGroupeType::class, $dossierPret);
        $form->handleRequest($request);

        $repoD = $this->getDoctrine()->getRepository(DossierPret::class);
        $familles = $repoD->findByFamilleDispo();


        if ($form->isSubmitted() && $form->isValid()) {

            $attestationAssurance1 = $form->get('attestationAssurance1')->getData();
            $carteEtudiant1 = $form->get('carteEtudiant1')->getData();
            $attestationAssurance2 = $form->get('attestationAssurance2')->getData();
            $carteEtudiant2 = $form->get('carteEtudiant2')->getData();
            $attestationAssurance3 = $form->get('attestationAssurance3')->getData();
            $carteEtudiant3 = $form->get('carteEtudiant3')->getData();
            $attestationAssurance4 = $form->get('attestationAssurance4')->getData();
            $carteEtudiant4 = $form->get('carteEtudiant4')->getData();
            $idFamille = $request->request->get('familles');

            $repoStatut = $this->getDoctrine()->getRepository(StatutDossier::class);
            $statut = $repoStatut->find(1);
            $dossierPret->setIdStatut($statut);
            $dossierPret->setIdDureePret(null);

            $repoUser = $this->getDoctrine()->getRepository(Etudiant::class);

            $user = $this->security->getUser();
            $connexion = $repoUser->find($user->getId());
            $dossierPret->setIdEtudiant($connexion);

            $repoFamille = $this->getDoctrine()->getRepository(Famille::class);
            $famille = $repoFamille->find($idFamille);
            $dossierPret->setIdFamille($famille);

            $dossierPret->setGroupe(true);

            $date = date("d/m/Y");
            $dossierPret->setDateDemande(\DateTime::createFromFormat('d/m/Y', $date));

            $dossierPret->setAttestationAssurance1($this->nomPDF($attestationAssurance1, "attestation"));
            $dossierPret->setCarteEtudiant1($this->nomPDF($carteEtudiant1, "carte"));
            if($attestationAssurance2 != null && $carteEtudiant2 != null){
                $dossierPret->setAttestationAssurance2($this->nomPDF($attestationAssurance2, "attestation"));
                $dossierPret->setCarteEtudiant2($this->nomPDF($carteEtudiant2, "carte2"));
            }
            if($attestationAssurance3 != null && $carteEtudiant3 != null) {

                $dossierPret->setAttestationAssurance3($this->nomPDF($attestationAssurance3, "attestation"));
                $dossierPret->setCarteEtudiant3($this->nomPDF($carteEtudiant3, "carte3"));
            }
            if($attestationAssurance4 != null && $carteEtudiant4 != null){
                $dossierPret->setAttestationAssurance4($this->nomPDF($attestationAssurance4, "attestation"));
                $dossierPret->setCarteEtudiant4($this->nomPDF($carteEtudiant4, "carte4"));
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($dossierPret);
            $entityManager->flush();

            return $this->redirectToRoute('dossier_pret_mon_dossier'); // A CHANGER

        }

        return $this->render('dossier_pret/newgroupe.html.twig', [
            'dossier_pret' => $dossierPret,
            'familles' => $familles,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/show", name="dossier_pret_show", methods={"GET"})
     */
    public function show(DossierPret $dossierPret): Response
    {
        return $this->render('dossier_pret/show.html.twig', [
            'dossier_pret' => $dossierPret,
        ]);
    }

    /**
     * @Route("/etudiant/{id}/edit", name="dossier_pret_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, DossierPret $dossierPret): Response
    {
        $user = $this->security->getUser();
        if($user !== $dossierPret->getIdEtudiant()){
            return $this->redirectToRoute('dossier_pret_mon_dossier');
        }
        $form = $this->createForm(DossierPretTypeEdit::class, $dossierPret);
        $form->handleRequest($request);
        $repoD = $this->getDoctrine()->getRepository(DossierPret::class);
        $familles = $repoD->findByFamilleDispo();
        if ($form->isSubmitted() && $form->isValid()) {
            $modification = $this->getDoctrine()->getRepository(StatutDossier::class)->find(9);
            $dossierPret->setIdStatut($modification);
            $attestationAssurance1 = $form->get('attestationAssurance1')->getData();
            $carteEtudiant1 = $form->get('carteEtudiant1')->getData();
            if ($attestationAssurance1){
                $dossierPret->setAttestationAssurance1($this->nomPDF($attestationAssurance1, "attestation"));
            }
            if($carteEtudiant1){
                $dossierPret->setCarteEtudiant1($this->nomPDF($carteEtudiant1, "carte"));
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('dossier_pret_mon_dossier');
        }

        return $this->render('dossier_pret/edit.html.twig', [
            'familles' => $familles,
            'dossier_pret' => $dossierPret,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/etudiant/{id}/edit/groupe", name="dossier_pret_edit_groupe", methods={"GET","POST"})
     */
    public function editGroupe(Request $request, DossierPret $dossierPret): Response
    {
        $user = $this->security->getUser();
        if($user !== $dossierPret->getIdEtudiant()){
            return $this->redirectToRoute('dossier_pret_mon_dossier');
        }
        $form = $this->createForm(DossierPretGroupeTypeEdit::class, $dossierPret);
        $form->handleRequest($request);
        $repoD = $this->getDoctrine()->getRepository(DossierPret::class);
        $familles = $repoD->findByFamilleDispo();
        if ($form->isSubmitted() && $form->isValid()) {
            $modification = $this->getDoctrine()->getRepository(StatutDossier::class)->find(9);
            $dossierPret->setIdStatut($modification);
            $attestationAssurance1 = $form->get('attestationAssurance1')->getData();
            $attestationAssurance1 = $form->get('attestationAssurance1')->getData();
            $carteEtudiant1 = $form->get('carteEtudiant1')->getData();
            $attestationAssurance2 = $form->get('attestationAssurance2')->getData();
            $carteEtudiant2 = $form->get('carteEtudiant2')->getData();
            $attestationAssurance3 = $form->get('attestationAssurance3')->getData();
            $carteEtudiant3 = $form->get('carteEtudiant3')->getData();
            $attestationAssurance4 = $form->get('attestationAssurance4')->getData();
            $carteEtudiant4 = $form->get('carteEtudiant4')->getData();
            $carteEtudiant1 = $form->get('carteEtudiant1')->getData();
            if ($attestationAssurance1){
                $dossierPret->setAttestationAssurance1($this->nomPDF($attestationAssurance1, "attestation"));
            }
            if($carteEtudiant1) {
                $dossierPret->setCarteEtudiant1($this->nomPDF($carteEtudiant1, "carte"));
            }
            if($attestationAssurance2 != null && $carteEtudiant2 != null){
                $dossierPret->setAttestationAssurance2($this->nomPDF($attestationAssurance2, "attestation"));
                $dossierPret->setCarteEtudiant2($this->nomPDF($carteEtudiant2, "carte2"));
            }
            if($attestationAssurance3 != null && $carteEtudiant3 != null) {

                $dossierPret->setAttestationAssurance3($this->nomPDF($attestationAssurance3, "attestation"));
                $dossierPret->setCarteEtudiant3($this->nomPDF($carteEtudiant3, "carte3"));
            }
            if($attestationAssurance4 != null && $carteEtudiant4 != null){
                $dossierPret->setAttestationAssurance4($this->nomPDF($attestationAssurance4, "attestation"));
                $dossierPret->setCarteEtudiant4($this->nomPDF($carteEtudiant4, "carte4"));
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('dossier_pret_mon_dossier');
        }

        return $this->render('dossier_pret/editgroupe.html.twig', [
            'familles' => $familles,
            'dossier_pret' => $dossierPret,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="dossier_pret_delete", methods={"DELETE"})
     */
    public function delete(Request $request, DossierPret $dossierPret): Response
    {
        if ($this->isCsrfTokenValid('delete'.$dossierPret->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($dossierPret);
            $entityManager->flush();
        }

        return $this->redirectToRoute('dossier_pret_index');
    }

    /**
     * @Route("/etudiant/mon/dossier", name="dossier_pret_mon_dossier", methods={"GET"})
     */
    public function mondossier(Request $request): Response
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $repoDossier = $this->getDoctrine()->getRepository(DossierPret::class);
        if(in_array('ROLE_ETUDIANT', $user->getRoles())) {
            $dossier_prets = $repoDossier->findByIdEtudiant($user->getId());
        } elseif (in_array('ROLE_PROFESSEUR', $user->getRoles())) {
            $dossier_prets = $repoDossier->findByIdProfesseur($user->getId());
        }

        return $this->render('dossier_pret/mondossier.html.twig', [
            'dossier_prets' => $dossier_prets,
        ]);
    }

    /**
     * @Route("/informatique/affectation", name="dossier_pret_affectation", methods={"GET"})
     */
    public function affectation(): Response
    {
        $dossierPrets = $this->getDoctrine()->getRepository(DossierPret::class)->findByIdStatut("4");
        return $this->render('dossier_pret/affectation.html.twig', [
            'dossier_prets' => $dossierPrets,
        ]);
    }

    /**
     * @Route("/informatique/listEquipement/{famille}", name="dossier_pret_informatique_list", methods={"GET"})
     */
    public function listEquipement(Request $request, String $famille) {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $sql = '
        SELECT t1.`nom` AS nom_famille, t2.nom AS nom_materiel, t2.description, 
        t3.`id` AS id_equipement, t3.`code_barre`
        FROM `famille` as t1
        LEFT JOIN `type_materiel` as t2 on t2.`id_famille` = t1.id
        LEFT JOIN (SELECT `id`, `id_type_materiel`, `code_barre`, `id_statut_materiel`
                   FROM `equipement`
                   WHERE `id_statut_materiel` = 2) as t3 on t3.`id_type_materiel` = t2.id
        WHERE t1.`nom` = :famille and t3.`code_barre` is not null
        GROUP BY t1.`nom`, t2.nom, t2.description, t3.`id`, t3.`code_barre`, t3.`id_statut_materiel`
        ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['famille' => $famille]);

        $result = $stmt->fetchAll();

        return new JsonResponse($result);
    }

    /**
     * @Route("/equipement/{idDossier}/{idEquipement}", name="dossier_pret_equipement", methods={"GET","POST"})
     */
    public function equipement(Request $request, Int $idDossier, Int $idEquipement, MailerInterface $mailer){
        $entityManager = $this->getDoctrine()->getManager();

        $repoDossier = $this->getDoctrine()->getRepository(DossierPret::class); 
        $repoDossierStatut = $this->getDoctrine()->getRepository(StatutDossier::class); 
        $repoStatutMateriel = $this->getDoctrine()->getRepository(StatutMateriel::class); 
        $repoEquipement = $this->getDoctrine()->getRepository(Equipement::class); 

        //trouver le dossier
        $dossier = $repoDossier->find($idDossier);
        //ajout commentaire
        $dossier->setCommentaireInformatique($request->request->get('commentaire'));
        //le dossier est complet
        $statutDossier = $repoDossierStatut->find(6);
        $dossier->setIdStatut($statutDossier);
        //l'équipement passe en statut "en prêt"
        $statutMateriel = $repoStatutMateriel->find(1);
        $equipement = $repoEquipement->find($idEquipement);
        $equipement->setIdStatutMateriel($statutMateriel);
        $entityManager->persist($equipement); 
        $entityManager->flush(); 
        //le dossier prend l'équipement
        $dossier->setIdEquipement($equipement);
        $entityManager->persist($dossier); 
        $entityManager->flush();
        $this->mailToEtudiant($mailer, $dossier, 'email/attente_etudiant.html.twig');

        return new JsonResponse("salut");
    }


    /**
     * @Route("/informatique/attente/etudiant", name="dossier_pret_attente_etudiant", methods={"GET"})
     */

    public function attenteEtudiant(){
        $repoDossierStatut = $this->getDoctrine()->getRepository(StatutDossier::class); 
        $statutDossier = $repoDossierStatut->find(6);
        $repoDossier = $this->getDoctrine()->getRepository(DossierPret::class);
        $dossier_prets = $repoDossier->findByIdStatut($statutDossier);
        return $this->render('dossier_pret/attente.html.twig', [
            'dossier_prets' => $dossier_prets,
        ]);
    }

    /**
     * @Route("/duree/{idDossier}/{idDuree}", name="dossier_pret_duree", methods={"GET","POST"})
     */
    public function duree(Request $request, Int $idDuree, Int $idDossier, MailerInterface $mailer){
        $entityManager = $this->getDoctrine()->getManager();
        $repoDossier = $this->getDoctrine()->getRepository(DossierPret::class); 
        $repoDuree = $this->getDoctrine()->getRepository(DureePret::class); 
        $repoDossierStatut = $this->getDoctrine()->getRepository(StatutDossier::class);
        
        
        $duree = $repoDuree->find($idDuree);
        $dossier = $repoDossier->find($idDossier); 
        //le dossier est valider
        $statutDossier = $repoDossierStatut->find(4);
        $dossier->setIdStatut($statutDossier);
        $dossier->setIdDureePret($duree);
        $dossier->setCommentaireAdministration("");
        $entityManager->persist($dossier); 
        $entityManager->flush();
        $this->mailToInformatique($mailer, $dossier);

        return new JsonResponse("salut");
    }


    /**
     * @Route("/set/commentaire/{idDossier}", name="duree_pret_commentaire", methods={"POST"})
     */
    public function list(Request $request, Int $idDossier) {
        $entityManager = $this->getDoctrine()->getManager();
        $commentaire = $request->request->get('commentaire');
        $repoDossier = $this->getDoctrine()->getRepository(DossierPret::class); 
        $dossier = $repoDossier->find($idDossier);
        $dossier->setCommentaireAdministration($commentaire);
        $statut = $this->getDoctrine()->getRepository(StatutDossier::class)->find(5);
        $dossier->setIdStatut($statut);
        $entityManager->persist($dossier); 
        $entityManager->flush();
        return new JsonResponse("salut");
    }

    /**
     * @Route("/secretariat/set/commentaire/{idDossier}", name="duree_pret_commentaire_secretariat", methods={"POST"})
     */
    public function commentaire(Request $request, Int $idDossier, MailerInterface $mailer) {
        $entityManager = $this->getDoctrine()->getManager();
        $commentaire = $request->request->get('commentaire');
        $repoDossier = $this->getDoctrine()->getRepository(DossierPret::class);
        $dossier = $repoDossier->find($idDossier);
        $dossier->setCommentaireAdministration($commentaire);
        $statut = $this->getDoctrine()->getRepository(StatutDossier::class)->find(3);
        $dossier->setIdStatut($statut);
        $entityManager->persist($dossier);
        $entityManager->flush();
        $this->mailRejet($mailer, $dossier);
        return new JsonResponse("salut");
    }

    /**
     * @Route("/administration", name="dossier_pret_administration", methods={"GET"})
     */
    public function administration()
    {
        $user = $this->getUser();
        if($user != null) {
            if (in_array('ROLE_SECRETARIAT_MIAGE', $user->getRoles()) or in_array('ROLE_SECRETARIAT_ISRI', $user->getRoles())) {
                return $this->redirectToRoute('dossier_pret_dashboard_administration', array("page"=>"etudiant"));
            }
        }
        return $this->render('dossier_pret/administration.html.twig');
    }

    /**
     * @Route("/informatique", name="dossier_pret_informatique", methods={"GET"})
     */
    public function informatique()
    {
        return $this->render('dossier_pret/informatique.html.twig');
    }

    /**
     * @Route("/informatique/emprunt/{id}", name="dossier_pret_etudiant_emprunt", methods={"GET"})
     */
    public function empruntEtudiant(DossierPret $dossierPret): Response
    {
        $statut = $this->getDoctrine()
            ->getRepository(StatutDossier::class)->find(7);
        $dossierPret->setIdStatut($statut);

        $em = $this->getDoctrine()->getManager();
        $em->persist($dossierPret);
        $em->flush();

        return $this->redirectToRoute('dossier_pret_attente_etudiant');
    }

    /**
     * @Route("/informatique/retour", name="dossier_pret_retour", methods={"GET"})
     */
    public function enpret(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $repoDossier = $this->getDoctrine()->getRepository(DossierPret::class);
        $statut = $this->getDoctrine()->getRepository(StatutDossier::class)->find(7);
        $dossier_prets = $repoDossier->findByIdStatut($statut);

        return $this->render('dossier_pret/enpret.html.twig', [
            'dossier_prets' => $dossier_prets,
        ]);
    }

    /**
     * @Route("/informatique/retour/{id}", name="dossier_pret_etudiant_retour", methods={"GET"})
     */
    public function retour(Int $id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $repoDossier = $this->getDoctrine()->getRepository(DossierPret::class);
        $dossier = $repoDossier->find($id);
        $statut = $this->getDoctrine()->getRepository(StatutDossier::class)->find(8);
        $dossier->setIdStatut($statut);
        //statut dossier terminé
        $dossier_prets = $repoDossier->findByIdStatut($statut);
        //statut équipement passe en stock
        $statutEquipement = $this->getDoctrine()->getRepository(StatutMateriel::class)->find(2);
        $dossier->getIdEquipement()->setIdStatutMateriel($statutEquipement);
        $dossier->setArchiveEquipement($dossier->getIdEquipement()->getCodeBarre());
        $dossier->setIdEquipement(null);
        $em = $this->getDoctrine()->getManager();
        $em->persist($dossier);
        $em->flush();
        return $this->redirectToRoute('dossier_pret_retour');
    }


    /**
     * @Route("/terminer", name="dossier_pret_terminer", methods={"GET"})
     */
    public function terminer(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $repoDossier = $this->getDoctrine()->getRepository(DossierPret::class);
        $statut = $this->getDoctrine()->getRepository(StatutDossier::class)->find(8);
        $dossier_prets = $repoDossier->findByIdStatut($statut);

        return $this->render('dossier_pret/terminer.html.twig', [
            'dossier_prets' => $dossier_prets,
        ]);
    }

    public function mailRejet(MailerInterface $mailer, DossierPret $dossierPret) //REJET DE LA DEMANDE
    {
        $email = (new Email())
            ->from('no-reply@pret.miage-amiens.fr')
            ->to($this->selectPersonEmail($dossierPret))
            ->subject('[ApplicationPrêt] - Demande rejetée par le secrétariat')
            ->text('Votre demande de prêt a été rejeté par l\'administration');
        $mailer->send($email);
    }


    public function mailToSecretariat(MailerInterface $mailer, DossierPret $dossierPret) //NOUVELLE DEMANDE
    {
        $nomRole = '%"ROLE_SECRETARIAT_MIAGE"%';
        $checkFormation = $dossierPret->getFormation();
        if (strpos($checkFormation, 'ISRI') !== false) {
            $nomRole = '%"ROLE_SECRETARIAT_ISRI"%';;
        }

        $repoUser = $this->getDoctrine()->getRepository(User::class);
        $users = $repoUser->findByRolesValue($nomRole);

        foreach ($users as $u){
            $email = (new Email())
                ->from('no-reply@pret.miage-amiens.fr')
                ->to($u->getUsername()) //secrétaire
                ->subject('[ApplicationPrêt - Secrétariat] - Nouvelle demande')
                ->text('Une nouvelle demande de prêt est en attente de votre validation.');
            $mailer->send($email);
        }


        $email = (new Email())
            ->from('no-reply@pret.miage-amiens.fr')
            ->to($this->selectPersonEmail($dossierPret))
            ->subject('[ApplicationPrêt] - Nouvelle demande')
            ->text('Nous avons bien pris en compte votre demande. L\'administration vous enverra des notifications pour vous prévenir du statut de votre demande.');

        $mailer->send($email);
    }

    public function mailToResponsable(MailerInterface $mailer, DossierPret $dossierPret) //ACCEPTER PAR LE SECRETARIAT
    {
        $nomRole = '%"ROLE_RESPONSABLE_MIAGE"%';
        $checkFormation = $dossierPret->getFormation();
        // Ajout mail ISRI
        if (strpos($checkFormation, 'ISRI') !== false) {
            $nomRole = '%"ROLE_RESPONSABLE_ISRI"%';;
        }

        $repoUser = $this->getDoctrine()->getRepository(User::class);
        $users = $repoUser->findByRolesValue($nomRole);

        foreach ($users as $u){
            $email = (new Email())
                ->from('no-reply@pret.miage-amiens.fr')
                ->to($u->getUsername()) //RESPONSABLE
                ->subject('[ApplicationPrêt - Responsable] - Demande en attente')
                ->text('Une nouvelle demande de prêt est en attente de votre validation.');
            $mailer->send($email);
        }

        $email = (new Email())
            ->from('no-reply@pret.miage-amiens.fr')
            ->to($this->selectPersonEmail($dossierPret))
            ->subject('[ApplicationPrêt] - Demande acceptée')
            ->text('Votre demande de prêt a été acceptée par le secrétariat');


        $mailer->send($email);
    }

    public function mailToInformatique(MailerInterface $mailer, DossierPret $dossierPret) //ACCEPTER PAR LE SECRETARIAT
    {
        $nomRole = '%"ROLE_INFORMATIQUE"%';
        $checkFormation = $dossierPret->getFormation();
        if (strpos($checkFormation, 'ISRI') !== false) {
            $nomRole = '%"ROLE_INFORMATIQUE"%';;
        }

        $repoUser = $this->getDoctrine()->getRepository(User::class);
        $users = $repoUser->findByRolesValue($nomRole);

        foreach ($users as $u){
            $email = (new Email())
                ->from('no-reply@pret.miage-amiens.fr')
                ->to($u->getUsername()) //INFORMATIQUE
                ->subject('[ApplicationPrêt - Informatique] - Demande en attente')
                ->text('Une nouvelle demande de prêt est en attende d\'affectation d\'équipement.');
            $mailer->send($email);
        }


        $email = (new Email())
            ->from('no-reply@pret.miage-amiens.fr')
            ->to($this->selectPersonEmail($dossierPret))
            ->subject('[ApplicationPrêt] - Demande acceptée par le responsable')
            ->text('Votre demande de prêt est passé en attente d\'affectation d\'équipement');
        $mailer->send($email);
    }

    public function mailToEtudiant(MailerInterface $mailer, DossierPret $dossierPret, string $template) //ACCEPTER PAR LE SECRETARIAT
    {

        $email = (new TemplatedEmail())
            ->from('no-reply@pret.miage-amiens.fr')
            ->to($this->selectPersonEmail($dossierPret))
            ->subject('[ApplicationPrêt] - Matériel disponible au service informatique')

            // path of the Twig template to render
            ->htmlTemplate($template)


            ->context([
                'dossierPret' => $dossierPret,
            ])
        ;
        $mailer->send($email);
    }

    /**
     * @Route("/redirect", name="dossier_pret_redirect", methods={"GET"})
     */
    public function redirectToPage()
    {
        $user = $this->getUser();
        if($user != null){
            if(in_array('ROLE_ADMIN', $user->getRoles())){
                return $this->redirectToRoute('dossier_pret_administration');
            }else if(in_array('ROLE_SECRETARIAT_MIAGE', $user->getRoles())) {
                return $this->redirectToRoute('dossier_pret_secretariat');
            }else if(in_array('ROLE_SECRETARIAT_ISRI', $user->getRoles())){
                return $this->redirectToRoute('dossier_pret_secretariat');
            }else if(in_array('ROLE_RESPONSABLE', $user->getRoles())){
                return $this->redirectToRoute('dossier_pret_responsable');
            }else if(in_array('ROLE_INFORMATIQUE', $user->getRoles())){
                return $this->redirectToRoute('dossier_pret_informatique');
            }else if(in_array('ROLE_ETUDIANT', $user->getRoles())) {
                return $this->redirectToRoute('dossier_pret_new');
            }else if(in_array('ROLE_PROFESSEUR', $user->getRoles())) {
                return $this->redirectToRoute('dossier_pret_new');
            }
        }

        return $this->redirectToRoute('index');
    }

    /**
     * @Route("/pdf/{idDossier}", name="dossier_pret_pdf", methods={"GET"})
     */
    public function getPDF(int $idDossier)
    {
        $repoDossier =  $this->getDoctrine()->getRepository(DossierPret::class);
        $dossierPret = $repoDossier->find($idDossier);

        $filename = "resume".$dossierPret->getIdEtudiant()->getNom().$dossierPret->getIdEtudiant()->getPrenom().'.pdf';

        $html = $this->renderView('dossier_pret/pdf.html.twig', array(
            'dossier_pret'  => $dossierPret
        ));

        return new PdfResponse(
            $this->manager->getOutputFromHtml($html),
            $filename
        );
    }

    /**
     * @Route("/informatique/dashboard/{page}", name="dossier_pret_dashboard", methods={"GET"})
     */
    public function dashboard(String $page)
    {
        return $this->render('dossier_pret/informatique.html.twig', [
            'page' => $page,
        ]);
    }

    /**
     * @Route("/administration/dashboard/{page}", name="dossier_pret_dashboard_administration", methods={"GET", "POST"})
     */
    public function dashboardAdministration(String $page)
    {
        return $this->render('dossier_pret/administration.html.twig', [
            'page' => $page,
        ]);
    }

    /**
     * @param DossierPret $dossierPret
     *
     * @return string
     */
    public function selectPersonEmail(DossierPret $dossierPret) {

        $result = "";
        if ($dossierPret->getIdEtudiant() !== null) {
            $result = $dossierPret->getIdEtudiant()->getEmail();
        } elseif ($dossierPret->getIdProfesseur() !== null) {
            $result = $dossierPret->getIdProfesseur()->getEmail();
        }

        return $result;
    }

    /**
     * @Route("/noequipement/{idDossier}", name="dossier_pret_equipement_non_affecte", methods={"GET","POST"})
     */
    public function equipementNoAffect(Request $request, DossierPret $idDossier, MailerInterface $mailer){
        $entityManager = $this->getDoctrine()->getManager();

        $repoDossier = $this->getDoctrine()->getRepository(DossierPret::class);
        $repoDossierStatut = $this->getDoctrine()->getRepository(StatutDossier::class);
        $repoStatutMateriel = $this->getDoctrine()->getRepository(StatutMateriel::class);

        //trouver le dossier
        $dossier = $repoDossier->find($idDossier);
        //le dossier est complet
        $statutDossier = $repoDossierStatut->find(6);
        $dossier->setIdStatut($statutDossier);
        //l'équipement passe en statut "en prêt"
        $statutMateriel = $repoStatutMateriel->find(1);
        $entityManager->flush();
        $this->mailToEtudiant($mailer, $dossier, 'email/attente_etudiant_no_equipment.html.twig');

        return $this->redirectToRoute('dossier_pret_affectation');
    }

    /**
     * @Route("/equipementafter/{idDossier}/{idEquipement}", name="dossier_pret_after_equipement", methods={"GET","POST"})
     */
    public function equipementAfterRDV(Request $request, Int $idDossier, Int $idEquipement, MailerInterface $mailer){
        $entityManager = $this->getDoctrine()->getManager();

        $repoDossier = $this->getDoctrine()->getRepository(DossierPret::class);
        $repoDossierStatut = $this->getDoctrine()->getRepository(StatutDossier::class);
        $repoStatutMateriel = $this->getDoctrine()->getRepository(StatutMateriel::class);
        $repoEquipement = $this->getDoctrine()->getRepository(Equipement::class);

        //trouver le dossier
        $dossier = $repoDossier->find($idDossier);
        //ajout commentaire
        $dossier->setCommentaireInformatique($request->request->get('commentaire'));
        //le dossier est complet
        $statutDossier = $repoDossierStatut->find(6);
        $dossier->setIdStatut($statutDossier);
        //l'équipement passe en statut "en prêt"
        $statutMateriel = $repoStatutMateriel->find(1);
        $equipement = $repoEquipement->find($idEquipement);
        $equipement->setIdStatutMateriel($statutMateriel);
        $entityManager->persist($equipement);
        $entityManager->flush();
        //le dossier prend l'équipement
        $dossier->setIdEquipement($equipement);
        $entityManager->persist($dossier);
        $entityManager->flush();

        return new JsonResponse("salut");
    }
}
