<?php

namespace App\Controller;

use App\Entity\DossierPret;
use App\Entity\Equipement;
use App\Entity\StatutMateriel;
use App\Form\StatutMaterielType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/statut/materiel")
 */
class StatutMaterielController extends AbstractController
{
    /**
     * @Route("/", name="statut_materiel_index", methods={"GET"})
     */
    public function index(): Response
    {
        $statutMateriels = $this->getDoctrine()
            ->getRepository(StatutMateriel::class)
            ->findAll();

        return $this->render('statut_materiel/index.html.twig', [
            'statut_materiels' => $statutMateriels,
        ]);
    }

    /**
     * @Route("/new", name="statut_materiel_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $statutMateriel = new StatutMateriel();
        $form = $this->createForm(StatutMaterielType::class, $statutMateriel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($statutMateriel);
            $entityManager->flush();

            return $this->redirectToRoute('dossier_pret_dashboard', array("page"=>"statut_materiel"));
        }

        return $this->render('statut_materiel/new.html.twig', [
            'statut_materiel' => $statutMateriel,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="statut_materiel_show", methods={"GET"})
     */
    public function show(StatutMateriel $statutMateriel): Response
    {
        return $this->render('statut_materiel/show.html.twig', [
            'statut_materiel' => $statutMateriel,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="statut_materiel_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, StatutMateriel $statutMateriel): Response
    {
        $form = $this->createForm(StatutMaterielType::class, $statutMateriel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('dossier_pret_dashboard', array("page"=>"statut_materiel"));
        }

        return $this->render('statut_materiel/edit.html.twig', [
            'statut_materiel' => $statutMateriel,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="statut_materiel_delete", methods={"DELETE"})
     */
    public function delete(Request $request, StatutMateriel $statutMateriel): Response
    {
        $this->get('session')->getFlashBag()->get('warning');
        $check = null;
        if ($this->isCsrfTokenValid('delete'.$statutMateriel->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $check = $entityManager->getRepository(Equipement::class)->findByIdStatutMateriel($statutMateriel);
            if($check == null) {
                $entityManager->remove($statutMateriel);
                $entityManager->flush();
            }else{
                $this->addFlash('warning', 'Erreur: il existe un ou plusieurs équipements avec ce statut');
                return $this->redirectToRoute('statut_materiel_edit', ["id" => $statutMateriel->getId()]);
            }


        }

        return $this->redirectToRoute('dossier_pret_dashboard', array("page"=>"statut_materiel"));
    }




}
