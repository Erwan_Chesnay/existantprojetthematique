<?php

namespace App\Controller;

use App\Entity\Etudiant;
use App\Entity\User;
use App\Form\EtudiantType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


use Symfony\Bridge\Twig\Mime\TemplatedEmail;

class EtudiantController extends AbstractController
{
    const MESSAGE = 'Message à rentrer par Anne LAPUJADE';

    /**
     * @Route("/etudiant", name="etudiant")
     */
    public function index()
    {
        return $this->render('etudiant/index.html.twig', [
            'controller_name' => 'EtudiantController',
        ]);
    }

    /**
     * @Route("/supprimer/{id}", name="etudiant_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Etudiant $etudiant)
    {
        if ($this->isCsrfTokenValid('delete'.$etudiant->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($etudiant);
            $entityManager->flush();
        }

        return $this->redirectToRoute('dossier_pret_dashboard_administration', array("page"=>"activation"));

    }


    /**
     * @Route("/etudiant/form", name="etudiant_form")
     */
    public function registration(Request $request, UserPasswordEncoderInterface $passwordEncoder, MailerInterface $mailer)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $etudiant = new Etudiant();
        $form = $this->createForm(EtudiantType::class, $etudiant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $userExist = $entityManager->getRepository(Etudiant::class)->findByEmail($form->get('email')->getData());
            if($userExist){
                $message = "Un compte existe déjà avec cette adresse-mail";
                return $this->render('etudiant/registration.html.twig', [
                    'message' => $message,
                    'etudiant' => $etudiant,
                    'registrationForm' => $form->createView(),
                ]);
            }else{
                $etudiant->setPassword(
                    $passwordEncoder->encodePassword(
                        $etudiant,
                        $form->get('password')->getData()
                    )
                );
                $etudiant->setRoles(["ROLE_ETUDIANT"]);
                $etudiant->setActivate(false);
                $entityManager->persist($etudiant);
                $entityManager->flush();
                $this->mailToSecretaire($mailer, $etudiant);
                return $this->redirectToRoute('index');
            }
        }

        $messageExplication = '';
        if ($user !== 'anon.') {
            if (in_array('ROLE_INFORMATIQUE', $user->getRoles())
                || in_array('ROLE_SECRETARIAT_MIAGE', $user->getRoles())
                || in_array('ROLE_SECRETARIAT_ISRI', $user->getRoles())
            ) {
                $messageExplication = self::MESSAGE;
            }
        }

        return $this->render('etudiant/registration.html.twig', [
            'etudiant' => $etudiant,
            'registrationForm' => $form->createView(),
            'messageExplication' => $messageExplication,
        ]);
    }

    /**
     * @Route("/etudiant/activate/{id}", name="etudiant_valide")
     */
    public function activate(int $id, MailerInterface $mailer){
        $repoEtudiant= $this->getDoctrine()->getRepository(Etudiant::class);
        $etudiant = $repoEtudiant->find($id);
        $etudiant->setActivate(true);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($etudiant);
        $entityManager->flush();

        $email = (new Email())
            ->from('no-reply@pret.miage-amiens.fr')
            ->to($etudiant->getUsername()) //ETUDIANT
            ->subject('[ApplicationPrêt] - Compte activé')
            ->text('Votre compte sur la plateforme de prêt Empruntica a été activé. Vous pouvez maintenant faire une demande.');
        $mailer->send($email);

        return $this->redirectToRoute('dossier_pret_dashboard_administration', array("page"=>"etudiant"));
    }

    /**
     * @Route("/etudiant/listeActivate", name="etudiant_liste_activate")
     */
    public function listeActivate(){
        $etudiants = $this->getDoctrine()
            ->getRepository(Etudiant::class)
            ->findByActivate(false);

        return $this->render('etudiant/list.html.twig', [
            'etudiants' => $etudiants,
        ]);
    }

    /**
     * @Route("/etudiant/liste", name="etudiant_liste")
     */
    public function liste(){
        $etudiants = $this->getDoctrine()
            ->getRepository(Etudiant::class)
            ->findByActivate(true);

        return $this->render('etudiant/list.html.twig', [
            'etudiants' => $etudiants,
        ]);
    }

    public function mailToSecretaire(MailerInterface $mailer, Etudiant $etudiant) //DEMANDE ACCES
    {
        $nomRole = '%"ROLE_SECRETARIAT_MIAGE"%';
        $checkFormation = $etudiant->getFormation();
        if (strpos($checkFormation, 'ISRI') !== false) {
            $nomRole = '%"ROLE_SECRETARIAT_ISRI"%';;
        }

        $repoUser = $this->getDoctrine()->getRepository(User::class);
        $users = $repoUser->findByRolesValue($nomRole);

        foreach ($users as $u){
            $email = (new TemplatedEmail())
                ->from('no-reply@pret.miage-amiens.fr')
                ->to($u->getUsername()) //SECRETAIRE
                ->subject('[ApplicationPrêt - Secrétariat] - Demande d\'accès')
                ->htmlTemplate('email/nouveau_etudiant.html.twig')
                ->context([
                    'etudiant' => $etudiant,
                ]);
            $mailer->send($email);
        }


        $mailer->send($email);
    }


}
