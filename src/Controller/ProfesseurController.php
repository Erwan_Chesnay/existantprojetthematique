<?php
namespace App\Controller;


use App\Entity\Professeur;
use App\Form\ProfesseurType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ProfesseurController extends AbstractController
{

    /**
     * @Route("/professeur/liste", name="professeur_liste")
     */
    public function liste()
    {
        $professeur = $this->getDoctrine()
            ->getRepository(Professeur::class)
            ->findAll();

        return $this->render('professeur/list.html.twig', [
            'professeurs' => $professeur,
        ]);
    }

    /**
     * @Route("/professeur/activate/{id}", name="professeur_valide")
     */
    public function activate(int $id, MailerInterface $mailer){
        $repoProfesseur= $this->getDoctrine()->getRepository(Professeur::class);
        $professeur = $repoProfesseur->find($id);
        $professeur->setActivate(true);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($professeur);
        $entityManager->flush();

        $email = (new Email())
            ->from('no-reply@pret.miage-amiens.fr')
            ->to($professeur->getUsername()) //Professeur
            ->subject('[ApplicationPrêt] - Compte activé')
            ->text('Votre compte sur la plateforme de prêt Empruntica a été activé. Vous pouvez maintenant faire une demande.');
        $mailer->send($email);

        return $this->redirectToRoute('dossier_pret_dashboard_administration', array("page"=>"professeur"));
    }

    /**
     * @Route("/professeur/supprimer/{id}", name="professeur_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Professeur $professeur)
    {
        if ($this->isCsrfTokenValid('delete'.$professeur->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($professeur);
            $entityManager->flush();
        }

        return $this->redirectToRoute('dossier_pret_dashboard_administration', array("page"=>"professeur"));

    }

    /**
     * @Route("/professeur/form", name="professeur_form")
     */
    public function registration(Request $request, UserPasswordEncoderInterface $passwordEncoder, MailerInterface $mailer)
    {
        $professeur = new Professeur();
        $form = $this->createForm(ProfesseurType::class, $professeur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $userExist = $entityManager->getRepository(Professeur::class)->findByEmail($form->get('email')->getData());
            if($userExist){
                $message = "Un compte existe déjà avec cette adresse-mail";
                return $this->render('professeur/registration.html.twig', [
                    'message' => $message,
                    'professeur' => $professeur,
                    'registrationForm' => $form->createView(),
                ]);
            }else{
                $professeur->setPassword(
                    $passwordEncoder->encodePassword(
                        $professeur,
                        $form->get('password')->getData()
                    )
                );
                $professeur->setRoles(["ROLE_PROFESSEUR"]);
                $this->activateAfterCreation($professeur, $form->get('password')->getData(), $mailer);

                $entityManager->persist($professeur);
                $entityManager->flush();

                return $this->redirectToRoute('dossier_pret_dashboard_administration', array("page"=>"professeur"));
            }
        }

        return $this->render('professeur/registration.html.twig', [
            'professeur' => $professeur,
            'registrationForm' => $form->createView(),
        ]);
    }

    public function activateAfterCreation(Professeur $professeur, string $password, MailerInterface $mailer)
    {
        $professeur->setActivate(true);
        $email = (new Email())
            ->from('no-reply@pret.miage-amiens.fr')
            ->to($professeur->getUsername()) //Professeur
            ->subject('[ApplicationPrêt] - Compte activé')
            ->text(
                'Votre compte sur la plateforme de prêt Empruntica a été activé. Vous pouvez maintenant faire une demande.
                Connectez vous avec votre mail courant et le mot de passe suivant : ' . $password
            );
        $mailer->send($email);
    }
}