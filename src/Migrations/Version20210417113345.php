<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210417113345 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'create the table for teachers';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('
            CREATE TABLE `professeur` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
              `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
              `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
              `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
              `roles` json NOT NULL,
              `activate` tinyint(1) NOT NULL DEFAULT "0",
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ');

        $this->addSql('
            ALTER TABLE `dossier_pret`
              ADD `id_professeur` int(11) DEFAULT NULL
        ');

        $this->addSql('
            ALTER TABLE `dossier_pret`
            ADD CONSTRAINT contrainte_id_connexion_professeur 
            FOREIGN KEY (id_professeur) REFERENCES professeur(id)
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE dossier_pret DROP FOREIGN KEY contrainte_id_connexion_professeur');
        $this->addSql('ALTER TABLE dossier_pret DROP COLUMN id_professeur;');
        $this->addSql('DROP TABLE professeur');
    }
}
