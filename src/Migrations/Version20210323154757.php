<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210323154757 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add user in user';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //$this->addSql('ALTER TABLE utilisateur DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE relation_role_utilisateur DROP FOREIGN KEY contrainte_role;');
        $this->addSql('ALTER TABLE relation_role_utilisateur DROP FOREIGN KEY contrainte_utilisateur;');
        $this->addSql('DROP TABLE relation_role_utilisateur;');
        $this->addSql('DROP TABLE utilisateur;');
        $this->addSql('DROP TABLE role;');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('
             CREATE TABLE `relation_role_utilisateur` (
             `id` int(11) NOT NULL AUTO_INCREMENT,
             `id_role` int(11) NOT NULL,
             `id_utilisateur` int(11) NOT NULL,
             PRIMARY KEY (`id`),
             KEY `contrainte_role` (`id_role`),
             KEY `contrainte_utilisateur` (`id_utilisateur`),
             CONSTRAINT `contrainte_role` FOREIGN KEY (`id_role`) REFERENCES `role` (`id`),
             CONSTRAINT `contrainte_utilisateur` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id`)
            )
        ');

        $this->addSql('
             CREATE TABLE `utilisateur` (
             `id` int(11) NOT NULL AUTO_INCREMENT,
             `identifiant` varchar(32) NOT NULL,
             `mot_de_passe` varchar(32) NOT NULL,
             `mail` text NOT NULL,
             `prenom` varchar(32) NOT NULL,
             `nom` varchar(32) NOT NULL,
             PRIMARY KEY (`id`)
            )
        ');

        $this->addSql('
             CREATE TABLE `role` (
             `id` int(11) NOT NULL AUTO_INCREMENT,
             `role_Name` varchar(32) NOT NULL,
             `role_Desc` text NOT NULL,
             PRIMARY KEY (`id`)
            )
        ');
    }
}
