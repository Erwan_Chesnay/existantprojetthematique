<?php

namespace App\Form;

use App\Entity\DossierPret;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Security\Core\Security;



class DossierPretType extends AbstractType
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $this->security->getUser();
        $builder
            ->add('commentaireEtudiant')
            ->add('dateFinSouhaite', DateType::class, ['widget' => 'single_text',]);

        if(in_array('ROLE_ETUDIANT', $user->getRoles())) {
            $builder
                ->add('formation', ChoiceType::class, [
                'choices'  => [
                    'M1 MIAGE' => "M1 MIAGE",
                    'M1 ISRI' => "M1 ISRI",
                    'M2 MIAGE - OSIE' => "M2 MIAGE - OSIE",
                    'M2 MIAGE - SIO' => "M2 MIAGE - SIO",
                    'M2 MIAGE - SID' => "M2 MIAGE - SID",
                    'M2 MIAGE - INE' => "M2 MIAGE - INE",
                    'M2 ISRI' => "M2 ISRI",
                ],
            ])
                ->add('attestationAssurance1', FileType::class, [
                    'label' => 'Attestation Assurance (fichier PDF)',

                    // unmapped means that this field is not associated to any entity property
                    'mapped' => false,

                    // make it optional so you don't have to re-upload the PDF file
                    // every time you edit the Product details
                    'required' => true,

                    // unmapped fields can't define their validation using annotations
                    // in the associated entity, so you can use the PHP constraint classes
                    'constraints' => [
                        new File([
                            'maxSize' => '4096k',
                            'mimeTypes' => [
                                'application/pdf',
                                'application/x-pdf',
                            ],
                            'mimeTypesMessage' => 'Fichier PDF non valide',
                        ])
                    ],
                ])
                ->add('carteEtudiant1', FileType::class, [
                    'label' => 'Carte étudiant (fichier PDF)',

                    // unmapped means that this field is not associated to any entity property
                    'mapped' => false,

                    // make it optional so you don't have to re-upload the PDF file
                    // every time you edit the Product details
                    'required' => true,

                    // unmapped fields can't define their validation using annotations
                    // in the associated entity, so you can use the PHP constraint classes
                    'constraints' => [
                        new File([
                            'maxSize' => '4096k',
                            'mimeTypes' => [
                                'application/pdf',
                                'application/x-pdf',
                            ],
                            'mimeTypesMessage' => 'Fichier PDF non valide',
                        ])
                    ],
                ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DossierPret::class,
        ]);
    }
}
