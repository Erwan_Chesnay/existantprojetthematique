<?php

namespace App\Form;

use App\Entity\Equipement;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EquipementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('etat')
            ->add('codeBarre', null, ['required' => true, 'label'=>'Prénom'])
            ->add('idAccesMateriel', null, ['required' => true, 'label'=>'Prénom'])
            ->add('idTypeMateriel', null, ['required' => true, 'label'=>'Prénom'])
            ->add('idStatutMateriel', null, ['required' => true, 'label'=>'Prénom'])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Equipement::class,
        ]);
    }
}
