<?php

namespace App\Form;

use App\Entity\Etudiant;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class EtudiantType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('prenom')
            ->add('numero')
            ->add('formation', ChoiceType::class, [
                'choices'  => [
                    'M1 MIAGE' => "M1 MIAGE",
                    'M1 ISRI' => "M1 ISRI",
                    'M2 MIAGE - OSIE' => "M2 MIAGE - OSIE",
                    'M2 MIAGE - SIO' => "M2 MIAGE - SIO",
                    'M2 MIAGE - SID' => "M2 MIAGE - SID",
                    'M2 MIAGE - INE' => "M2 MIAGE - INE",
                    'M2 ISRI' => "M2 ISRI",
                ],
            ])
            ->add('email', TextType::class, ['data' => '@etud.u-picardie.fr', 'label'=>'E-Mail'])
            ->add('password', PasswordType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Entrer un mot de passe',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Votre mot de passe doit faire au minimum {{ limit }} caractéres',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Etudiant::class,
        ]);
    }
}
