<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TypeMateriel
 *
 * @ORM\Table(name="type_materiel", indexes={@ORM\Index(name="contrainte_famille", columns={"id_famille"})})
 * @ORM\Entity
 */
class TypeMateriel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var \Famille
     *
     * @ORM\ManyToOne(targetEntity="Famille")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_famille", referencedColumnName="id")
     * })
     */
    private $idFamille;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIdFamille(): ?Famille
    {
        return $this->idFamille;
    }

    public function setIdFamille(?Famille $idFamille): self
    {
        $this->idFamille = $idFamille;

        return $this;
    }


    public function __toString() {
        $champ = $this->nom;
        $champ.=" - ";
        $champ.=$this->description;
        return $champ;
    }

}
