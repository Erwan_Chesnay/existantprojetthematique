<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DossierPret
 *
 * @ORM\Table(name="dossier_pret", indexes={@ORM\Index(name="id_connexion", columns={"id_connexion"}), @ORM\Index(name="contrainte_duree_pret", columns={"id_duree_pret"}), @ORM\Index(name="name_materiel", columns={"id_equipement"}), @ORM\Index(name="contrainte_id_dossier", columns={"id_statut"})})
 * @ORM\Entity(repositoryClass="App\Repository\DossierPretRepository")
 */
class DossierPret
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="attestation_assurance1", type="text", length=65535, nullable=false)
     */
    private $attestationAssurance1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="attestation_assurance2", type="text", length=65535, nullable=true)
     */
    private $attestationAssurance2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="attestation_assurance3", type="text", length=65535, nullable=true)
     */
    private $attestationAssurance3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="attestation_assurance4", type="text", length=65535, nullable=true)
     */
    private $attestationAssurance4;

    /**
     * @var string
     *
     * @ORM\Column(name="carte_etudiant1", type="text", length=65535, nullable=false)
     */
    private $carteEtudiant1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="carte_etudiant2", type="text", length=65535, nullable=true)
     */
    private $carteEtudiant2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="carte_etudiant3", type="text", length=65535, nullable=true)
     */
    private $carteEtudiant3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="carte_etudiant4", type="text", length=65535, nullable=true)
     */
    private $carteEtudiant4;

    /**
     * @var string
     *
     * @ORM\Column(name="formation", type="string", length=32, nullable=false)
     */
    private $formation;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_demande", type="date", nullable=false)
     */
    private $dateDemande;

    /**
     * @var string
     *
     * @ORM\Column(name="commentaire_etudiant", type="text", length=65535, nullable=false)
     */
    private $commentaireEtudiant;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_fin_souhaite", type="date", nullable=false)
     */
    private $dateFinSouhaite;

    /**
     * @var string|null
     *
     * @ORM\Column(name="commentaire_administration", type="text", length=65535, nullable=true)
     */
    private $commentaireAdministration;

    /**
     * @var string|null
     *
     * @ORM\Column(name="commentaire_informatique", type="text", length=65535, nullable=true)
     */
    private $commentaireInformatique;

    /**
     * @var string|null
     *
     * @ORM\Column(name="archive_equipement", type="text", length=255, nullable=true)
     */
    private $archiveEquipement;

    /**
     * @var \DureePret
     *
     * @ORM\ManyToOne(targetEntity="DureePret")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_duree_pret", referencedColumnName="id")
     * })
     */
    private $idDureePret;

    /**
     * @var \StatutDossier
     *
     * @ORM\ManyToOne(targetEntity="StatutDossier")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_statut", referencedColumnName="id")
     * })
     */
    private $idStatut;

    /**
     * @var \Equipement
     *
     * @ORM\ManyToOne(targetEntity="Equipement")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_equipement", referencedColumnName="id")
     * })
     */
    private $idEquipement;

    /**
     * @var \Etudiant
     *
     * @ORM\ManyToOne(targetEntity="Etudiant")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_etudiant", referencedColumnName="id")
     * })
     */
    private $idEtudiant;

    /**
     * @var \Professeur
     *
     * @ORM\ManyToOne(targetEntity="Professeur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_professeur", referencedColumnName="id")
     * })
     */
    private $idProfesseur;

    /**
     * @var \Famille
     *
     * @ORM\ManyToOne(targetEntity="Famille")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_famille", referencedColumnName="id")
     * })
     */
    private $idFamille;

    /**
     * @ORM\Column(type="boolean")
     */
    private $groupe;

    /**
     * @return mixed
     */
    public function getGroupe()
    {
        return $this->groupe;
    }

    /**
     * @param mixed $groupe
     */
    public function setGroupe($groupe): void
    {
        $this->groupe = $groupe;
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAttestationAssurance1(): ?string
    {
        return $this->attestationAssurance1;
    }

    public function setAttestationAssurance1(string $attestationAssurance1): self
    {
        $this->attestationAssurance1 = $attestationAssurance1;

        return $this;
    }

    public function getAttestationAssurance2(): ?string
    {
        return $this->attestationAssurance2;
    }

    public function setAttestationAssurance2(?string $attestationAssurance2): self
    {
        $this->attestationAssurance2 = $attestationAssurance2;

        return $this;
    }

    public function getAttestationAssurance3(): ?string
    {
        return $this->attestationAssurance3;
    }

    public function setAttestationAssurance3(?string $attestationAssurance3): self
    {
        $this->attestationAssurance3 = $attestationAssurance3;

        return $this;
    }

    public function getAttestationAssurance4(): ?string
    {
        return $this->attestationAssurance4;
    }

    public function setAttestationAssurance4(?string $attestationAssurance4): self
    {
        $this->attestationAssurance4 = $attestationAssurance4;

        return $this;
    }

    public function getCarteEtudiant1(): ?string
    {
        return $this->carteEtudiant1;
    }

    public function setCarteEtudiant1(string $carteEtudiant1): self
    {
        $this->carteEtudiant1 = $carteEtudiant1;

        return $this;
    }

    public function getCarteEtudiant2(): ?string
    {
        return $this->carteEtudiant2;
    }

    public function setCarteEtudiant2(?string $carteEtudiant2): self
    {
        $this->carteEtudiant2 = $carteEtudiant2;

        return $this;
    }

    public function getCarteEtudiant3(): ?string
    {
        return $this->carteEtudiant3;
    }

    public function setCarteEtudiant3(?string $carteEtudiant3): self
    {
        $this->carteEtudiant3 = $carteEtudiant3;

        return $this;
    }

    public function getCarteEtudiant4(): ?string
    {
        return $this->carteEtudiant4;
    }

    public function setCarteEtudiant4(?string $carteEtudiant4): self
    {
        $this->carteEtudiant4 = $carteEtudiant4;

        return $this;
    }

    public function getFormation(): ?string
    {
        return $this->formation;
    }

    public function setFormation(string $formation): self
    {
        $this->formation = $formation;

        return $this;
    }


    public function getDateDemande(): ?\DateTimeInterface
    {
        return $this->dateDemande;
    }

    public function setDateDemande(\DateTimeInterface $dateDemande): self
    {
        $this->dateDemande = $dateDemande;

        return $this;
    }

    public function getCommentaireEtudiant(): ?string
    {
        return $this->commentaireEtudiant;
    }

    public function setCommentaireEtudiant(string $commentaireEtudiant): self
    {
        $this->commentaireEtudiant = $commentaireEtudiant;

        return $this;
    }

    public function getDateFinSouhaite(): ?\DateTimeInterface
    {
        return $this->dateFinSouhaite;
    }

    public function setDateFinSouhaite(\DateTimeInterface $dateFinSouhaite): self
    {
        $this->dateFinSouhaite = $dateFinSouhaite;

        return $this;
    }

    public function getCommentaireAdministration(): ?string
    {
        return $this->commentaireAdministration;
    }

    public function setCommentaireAdministration(?string $commentaireAdministration): self
    {
        $this->commentaireAdministration = $commentaireAdministration;

        return $this;
    }

    public function getIdDureePret(): ?DureePret
    {
        return $this->idDureePret;
    }

    public function setIdDureePret(?DureePret $idDureePret): self
    {
        $this->idDureePret = $idDureePret;

        return $this;
    }

    public function getIdStatut(): ?StatutDossier
    {
        return $this->idStatut;
    }

    public function setIdStatut(?StatutDossier $idStatut): self
    {
        $this->idStatut = $idStatut;

        return $this;
    }

    public function getIdEquipement(): ?Equipement
    {
        return $this->idEquipement;
    }

    public function setIdEquipement(?Equipement $idEquipement): self
    {
        $this->idEquipement = $idEquipement;

        return $this;
    }

    public function getIdEtudiant(): ?Etudiant
    {
        return $this->idEtudiant;
    }

    public function setIdEtudiant(?Etudiant $idEtudiant): self
    {
        $this->idEtudiant = $idEtudiant;

        return $this;
    }

    public function getIdProfesseur(): ?Professeur
    {
        return $this->idProfesseur;
    }

    public function setIdProfesseur(?Professeur $idProfesseur): self
    {
        $this->idProfesseur = $idProfesseur;

        return $this;
    }

    public function getIdFamille(): ?Famille
    {
        return $this->idFamille;
    }

    public function setIdFamille(?Famille $idFamille): self
    {
        $this->idFamille = $idFamille;

        return $this;
    }

    public function getArchiveEquipement(): ?string
    {
        return $this->archiveEquipement;
    }

    public function setArchiveEquipement(string $archive_equipement): self
    {
        $this->archiveEquipement = $archive_equipement;

        return $this;
    }

    public function getCommentaireInformatique(): ?string
    {
        return $this->commentaireInformatique;
    }

    public function setCommentaireInformatique(string $commentaireInformatique): self
    {
        $this->commentaireInformatique = $commentaireInformatique;

        return $this;
    }


}
