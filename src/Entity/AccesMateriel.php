<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AccesMateriel
 *
 * @ORM\Table(name="acces_materiel")
 * @ORM\Entity
 */
class AccesMateriel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_acces", type="string", length=255, nullable=false)
     */
    private $nomAcces;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomAcces(): ?string
    {
        return $this->nomAcces;
    }

    public function setNomAcces(string $nomAcces): self
    {
        $this->nomAcces = $nomAcces;

        return $this;
    }

    public function __toString() {

        return $this->nomAcces;
    }



}
