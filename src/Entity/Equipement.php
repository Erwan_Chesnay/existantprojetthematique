<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Equipement
 *
 * @ORM\Table(name="equipement", indexes={@ORM\Index(name="contrainte_acces_materiel", columns={"id_acces_materiel"}), @ORM\Index(name="name_materiel", columns={"id_type_materiel"})})
 * @ORM\Entity
 */
class Equipement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \StatutMateriel
     *
     * @ORM\ManyToOne(targetEntity="StatutMateriel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_statut_materiel", referencedColumnName="id")
     * })
     */

    private $idStatutMateriel;

    /**
     * @var string
     *
     * @ORM\Column(name="etat", type="string", length=255, nullable=false)
     */
    private $etat;

    /**
     * @var string
     *
     * @ORM\Column(name="code_barre", type="text", length=65535, nullable=false)
     */
    private $codeBarre;

    /**
     * @var \AccesMateriel
     *
     * @ORM\ManyToOne(targetEntity="AccesMateriel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_acces_materiel", referencedColumnName="id")
     * })
     */
    private $idAccesMateriel;

    /**
     * @var \TypeMateriel
     *
     * @ORM\ManyToOne(targetEntity="TypeMateriel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_type_materiel", referencedColumnName="id")
     * })
     */
    private $idTypeMateriel;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdStatutMateriel(): ?StatutMateriel
    {
        return $this->idStatutMateriel;
    }

    public function setIdStatutMateriel(?StatutMateriel $idStatutMateriel): self
    {
        $this->idStatutMateriel = $idStatutMateriel;

        return $this;
    }

    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(string $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function getCodeBarre(): ?string
    {
        return $this->codeBarre;
    }

    public function setCodeBarre(string $codeBarre): self
    {
        $this->codeBarre = $codeBarre;

        return $this;
    }

    public function getIdAccesMateriel(): ?AccesMateriel
    {
        return $this->idAccesMateriel;
    }

    public function setIdAccesMateriel(?AccesMateriel $idAccesMateriel): self
    {
        $this->idAccesMateriel = $idAccesMateriel;

        return $this;
    }

    public function getIdTypeMateriel(): ?TypeMateriel
    {
        return $this->idTypeMateriel;
    }

    public function setIdTypeMateriel(?TypeMateriel $idTypeMateriel): self
    {
        $this->idTypeMateriel = $idTypeMateriel;

        return $this;
    }


}
