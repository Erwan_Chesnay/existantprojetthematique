<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StatutDossier
 *
 * @ORM\Table(name="statut_dossier")
 * @ORM\Entity
 */
class StatutDossier
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="statut_nom", type="string", length=32, nullable=false)
     */
    private $statutNom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="statut_desc", type="text", length=65535, nullable=true)
     */
    private $statutDesc;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatutNom(): ?string
    {
        return $this->statutNom;
    }

    public function setStatutNom(string $statutNom): self
    {
        $this->statutNom = $statutNom;

        return $this;
    }

    public function getStatutDesc(): ?string
    {
        return $this->statutDesc;
    }

    public function setStatutDesc(?string $statutDesc): self
    {
        $this->statutDesc = $statutDesc;

        return $this;
    }


}
