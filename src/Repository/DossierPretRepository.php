<?php

namespace App\Repository;

use App\Entity\DossierPret;
use App\Entity\Equipement;
use App\Entity\Famille;
use App\Entity\TypeMateriel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DossierPret|null find($id, $lockMode = null, $lockVersion = null)
 * @method DossierPret|null findOneBy(array $criteria, array $orderBy = null)
 * @method DossierPret[]    findAll()
 * @method DossierPret[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DossierPretRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DossierPret::class);
    }

    public function findBySecretariat()
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('dp')
            ->from('App:DossierPret', 'dp')
            ->where('dp.idStatut = 1'/*premiere demande*/)
            ->orWhere('dp.idStatut = 2'/*validation secretariat*/)
            ->orWhere('dp.idStatut = 3'/*rejet secretariat*/)
            ->orWhere('dp.idStatut = 7'/*en prêt*/);

        $query = $qb->getQuery();
        return $query->execute();
    }

    public function findBySecretariatMiage()
    {
        $em = $this->getEntityManager();

        $queryBuilder = $em->createQueryBuilder();

        $queryBuilder->select('dp')
            ->from('App:DossierPret', 'dp')
            ->where('dp.idStatut = 1')
            ->orWhere('dp.idStatut = 2')
            ->orWhere('dp.idStatut = 3')
            ->orWhere('dp.idStatut = 7')
            ->andWhere('dp.formation LIKE :FORMATION')
            ->setParameter('FORMATION', '%MIAGE%');
        return $queryBuilder->getQuery()->execute();

    }

    public function findBySecretariatIsri()
    {
        $em = $this->getEntityManager();

        $queryBuilder = $em->createQueryBuilder();

        $queryBuilder->select('dp')
            ->from('App:DossierPret', 'dp')
            ->where('dp.idStatut = 1')
            ->orWhere('dp.idStatut = 2')
            ->orWhere('dp.idStatut = 3')
            ->orWhere('dp.idStatut = 7')
            ->andWhere('dp.formation LIKE :FORMATION')
            ->setParameter('FORMATION', '%ISRI%');
        return $queryBuilder->getQuery()->execute();
    }

    public function findByResponsable()
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('dp')
            ->from('App:DossierPret', 'dp')
            ->where('dp.idStatut = 2'/*validation responsable*/)
            ->orWhere('dp.idStatut = 4'/*validé responsable*/)
            ->orWhere('dp.idStatut = 5'/*rejeté responsable*/)
            ->orWhere('dp.idStatut = 7'/*en prêt*/);

        $query = $qb->getQuery();
        return $query->execute();
    }

    public function findByResponsableMIAGE()
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('dp')
            ->from('App:DossierPret', 'dp')
            ->where('dp.idStatut = 2'/*validation responsable*/)
            ->orWhere('dp.idStatut = 4'/*validé responsable*/)
            ->orWhere('dp.idStatut = 5'/*rejeté responsable*/)
            ->orWhere('dp.idStatut = 7'/*en prêt*/)
            ->andWhere('dp.formation LIKE :FORMATION')
            ->setParameter('FORMATION', '%MIAGE%');

        $query = $qb->getQuery();
        return $query->execute();
    }

    public function findByResponsableISRI()
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('dp')
            ->from('App:DossierPret', 'dp')
            ->where('dp.idStatut = 2'/*validation responsable*/)
            ->orWhere('dp.idStatut = 4'/*validé responsable*/)
            ->orWhere('dp.idStatut = 5'/*rejeté responsable*/)
            ->orWhere('dp.idStatut = 7'/*en prêt*/)
            ->andWhere('dp.formation LIKE :FORMATION')
            ->setParameter('FORMATION', '%ISRI%');

        $query = $qb->getQuery();
        return $query->execute();
    }

    public function findByResponsablePersoUPJV()
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('dp')
            ->from('App:DossierPret', 'dp')
            ->where('dp.idStatut = 2'/*validation responsable*/)
            ->orWhere('dp.idStatut = 4'/*validé responsable*/)
            ->orWhere('dp.idStatut = 5'/*rejeté responsable*/)
            ->orWhere('dp.idStatut = 7'/*en prêt*/)
            ->andWhere('dp.formation LIKE :FORMATION')
            ->setParameter('FORMATION', 'Personnel UPJV');

        $query = $qb->getQuery();
        return $query->execute();
    }

    public function findByFamilleDispo()
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql ="SELECT tfin.id, CASE WHEN tfin.P_Dispo = 0 then CONCAT(tfin.nom, \" \",\"(plus disponible)\") else tfin.nom end as Disponible, tfin.P_Dispo
FROM (SELECT t1.id, t1.nom, t1.Nb_eq_Famille_demande, t2.Nb_eq_dispo , CASE WHEN Nb_eq_dispo <= t1.Nb_eq_Famille_demande THEN 0 ELSE 1 END AS P_Dispo
FROM (SELECT f.id, f.nom, CASE WHEN COUNT(dp.id_famille)='0' THEN 0 ELSE COUNT(dp.id_famille) END as Nb_eq_Famille_demande
	FROM famille as f 
	LEFT JOIN (SELECT *
               FROM dossier_pret 
               WHERE id_statut IN (1,2,4,6)) as dp on dp.id_famille = f.id
    LEFT JOIN statut_dossier as sd on dp.id_statut = sd.id
	group by f.id) as t1, (SELECT f.id, f.nom, CASE WHEN COUNT(*)='0' THEN 0 ELSE COUNT(e.id_statut_materiel) END as Nb_eq_dispo
                                        FROM famille f 
                                        LEFT JOIN type_materiel tm on tm.id_famille = f.id 
                                        LEFT join equipement e on e.id_type_materiel = tm.id AND e.id_statut_materiel = '2'
                                        GROUP BY f.id) as t2
WHERE t1.id = t2.id) as tfin";



        $stmt = $conn->prepare($sql);
        $stmt->execute();

        // returns an array of arrays (i.e. a raw data set)
        return $stmt->fetchAll();
    }
}
